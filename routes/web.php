<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});*/
// Auth
Route::get('/', 'AuthController@index');
Route::post('/auth/login', 'AuthController@login');

//Home
Route::get('/home', 'HomeController@index');

//Switch Siswa
Route::get('/changesiswa', 'ChangeSiswaController@index');
Route::get('/changesiswa/switchsiswa', 'ChangeSiswaController@SwitchSiswa');

//Logout
Route::get('/logout', 'LogoutController@index');

// Select List
Route::get('/selectlist/GetListLevel', 'SelectListController@GetListLevel');
Route::get('/selectlist/GetListSiswa', 'SelectListController@GetListSiswa');
Route::get('/selectlist/GetListTingkat', 'SelectListController@GetListTingkat');
Route::get('/selectlist/GetListSemester', 'SelectListController@GetListSemester');
Route::get('/selectlist/GetListAgama', 'SelectListController@GetListAgama');
Route::get('/selectlist/GetListGender', 'SelectListController@GetListGender');
Route::get('/selectlist/GetListKelas', 'SelectListController@GetListKelas');
Route::get('/selectlist/GetListMataPelajaran', 'SelectListController@GetListMataPelajaran');
Route::get('/selectlist/GetListHari', 'SelectListController@GetListHari');
Route::get('/selectlist/GetListSiswaByKelasCode', 'SelectListController@GetListSiswaByKelasCode');
Route::get('/selectlist/GetListSiswaByWaliMuridCode', 'SelectListController@GetListSiswaByWaliMuridCode');

// Master User
Route::get('/masteruser', 'MasterUserController@index');
Route::post('/masteruser/GetListData', 'MasterUserController@GetListData');
Route::post('/masteruser/Edit', 'MasterUserController@Edit');
Route::post('/masteruser/Delete', 'MasterUserController@Delete');
Route::post('/masteruser/Save', 'MasterUserController@Save');

// Master Guru
Route::get('/masterguru', 'MasterGuruController@index');
Route::post('/masterguru/GetListData', 'MasterGuruController@GetListData');
Route::post('/masterguru/Edit', 'MasterGuruController@Edit');
Route::post('/masterguru/Delete', 'MasterGuruController@Delete');
Route::post('/masterguru/Save', 'MasterGuruController@Save');

// Master Siswa
Route::get('/mastersiswa', 'MasterSiswaController@index');
Route::post('/mastersiswa/GetListData', 'MasterSiswaController@GetListData');
Route::post('/mastersiswa/Edit', 'MasterSiswaController@Edit');
Route::post('/mastersiswa/Delete', 'MasterSiswaController@Delete');
Route::post('/mastersiswa/Save', 'MasterSiswaController@Save');

// Master Wali Murid
Route::get('/masterwalimurid', 'MasterWaliMuridController@index');
Route::post('/masterwalimurid/GetListData', 'MasterWaliMuridController@GetListData');
Route::post('/masterwalimurid/GetListSiswaByWaliMuridId', 'MasterWaliMuridController@GetListSiswaByWaliMuridId');
Route::post('/masterwalimurid/GetListSiswaByWaliMuridCode', 'MasterWaliMuridController@GetListSiswaByWaliMuridCode');
Route::post('/masterwalimurid/Edit', 'MasterWaliMuridController@Edit');
Route::post('/masterwalimurid/Delete', 'MasterWaliMuridController@Delete');
Route::post('/masterwalimurid/DeleteSiswa', 'MasterWaliMuridController@DeleteSiswa');
Route::post('/masterwalimurid/Save', 'MasterWaliMuridController@Save');

// Master Kelas
Route::get('/masterkelas', 'MasterKelasController@index');
Route::post('/masterkelas/GetListData', 'MasterKelasController@GetListData');
Route::post('/masterkelas/GetListSiswaByKelasId', 'MasterKelasController@GetListSiswaByKelasId');
Route::post('/masterkelas/GetListSiswaByKelasCode', 'MasterKelasController@GetListSiswaByKelasCode');
Route::post('/masterkelas/Edit', 'MasterKelasController@Edit');
Route::post('/masterkelas/Delete', 'MasterKelasController@Delete');
Route::post('/masterkelas/DeleteSiswa', 'MasterKelasController@DeleteSiswa');
Route::post('/masterkelas/Save', 'MasterKelasController@Save');


// Master Mata Pelajaran
Route::get('/mastermatapelajaran', 'MasterMataPelajaranController@index');
Route::post('/mastermatapelajaran/GetListData', 'MasterMataPelajaranController@GetListData');
Route::post('/mastermatapelajaran/Edit', 'MasterMataPelajaranController@Edit');
Route::post('/mastermatapelajaran/Delete', 'MasterMataPelajaranController@Delete');
Route::post('/mastermatapelajaran/Save', 'MasterMataPelajaranController@Save');

// Trans Jadwal
Route::get('/transjadwal', 'TransJadwalController@index');
Route::post('/transjadwal/GetListData', 'TransJadwalController@GetListData');
Route::post('/transjadwal/GetListDetailByJadwalId', 'TransJadwalController@GetListDetailByJadwalId');
Route::post('/transjadwal/GetListDetailByJadwalCode', 'TransJadwalController@GetListDetailByJadwalCode');
Route::post('/transjadwal/Edit', 'TransJadwalController@Edit');
Route::post('/transjadwal/Delete', 'TransJadwalController@Delete');
Route::post('/transjadwal/DeleteDetail', 'TransJadwalController@DeleteDetail');
Route::post('/transjadwal/Save', 'TransJadwalController@Save');


// Trans Jurnal
Route::get('/transjurnal', 'TransJurnalController@index');
Route::post('/transjurnal/GetListData', 'TransJurnalController@GetListData');
Route::post('/transjurnal/Edit', 'TransJurnalController@Edit');
Route::post('/transjurnal/Delete', 'TransJurnalController@Delete');
Route::post('/transjurnal/Save', 'TransJurnalController@Save');


// Trans Kehadiran
Route::get('/transkehadiran', 'TransKehadiranController@index');
Route::post('/transkehadiran/GetListData', 'TransKehadiranController@GetListData');
Route::post('/transkehadiran/GetListDetailByKehadiranId', 'TransKehadiranController@GetListDetailByKehadiranId');
Route::post('/transkehadiran/Edit', 'TransKehadiranController@Edit');
Route::post('/transkehadiran/Delete', 'TransKehadiranController@Delete');
Route::post('/transkehadiran/DeleteDetail', 'TransKehadiranController@DeleteDetail');
Route::post('/transkehadiran/Save', 'TransKehadiranController@Save');


// Trans Nilai
Route::get('/transnilai', 'TransNilaiController@index');
Route::post('/transnilai/GetListData', 'TransNilaiController@GetListData');
Route::post('/transnilai/GetListDetailByNilaiId', 'TransNilaiController@GetListDetailByNilaiId');
Route::post('/transnilai/Edit', 'TransNilaiController@Edit');
Route::post('/transnilai/Delete', 'TransNilaiController@Delete');
Route::post('/transnilai/DeleteDetail', 'TransNilaiController@DeleteDetail');
Route::post('/transnilai/Save', 'TransNilaiController@Save');

// Trans Pengumuman
Route::get('/transpengumuman', 'TransPengumumanController@index');
Route::post('/transpengumuman/GetListData', 'TransPengumumanController@GetListData');
Route::post('/transpengumuman/Edit', 'TransPengumumanController@Edit');
Route::post('/transpengumuman/Delete', 'TransPengumumanController@Delete');
Route::post('/transpengumuman/Save', 'TransPengumumanController@Save');


