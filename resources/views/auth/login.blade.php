@include('frame._Header')
<body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <img class="login-image" src="{{asset ('assets/img/logo.png')}}" alt="logo"><br/>
        <a href="{{ url('/') }}"><b>{{ Session::get('app_name') }}</b></a>
      </div>
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">{{ Session::get('alert_messages') }}</p>
            <?php  Session::put('alert', false);
            Session::put('alert_types', null);
            Session::put('alert_messages', null); ?>
          <form action="{{ url('/auth/login') }}" method="post">
            @csrf
            <div class="input-group mb-3">
              <input type="text" id="username" name="username" class="form-control" placeholder="Nama Pengguna">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" id="password" name="password" class="form-control" placeholder="Kata Sandi">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <!-- /.col -->
              <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">Masuk</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="{{asset ('assets/plugins/jquery/jquery.min.js')}} "></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset ('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}} "></script>
    <!-- AdminLTE App -->
    <script src="{{asset ('assets/js/adminlte.min.js')}} "></script>

  </body>
