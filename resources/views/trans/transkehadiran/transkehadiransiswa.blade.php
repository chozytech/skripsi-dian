@include('frame._Header')
@include('frame.menu._HeaderMenu')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
       <div class="container-fluid">
          <div class="row mb-2">
             <div class="col-sm-6">
             </div>
             <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">{{ Session::get('page') }}</li>
                </ol>
             </div>
          </div>
       </div>
    </div><!-- /.container-fluid -->

    <!-- Main content -->
    <section class="content">
       <div class="row">
          <div class="col-12">
             <div class="card">
                <div class="card-header">
                   <h3 class="card-title">{{ Session::get('page') }} </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <input id="session_level" value="{{ Session::get('level_id') }}" hidden />

                   <table id="tbdata" class="table table-striped table-bordered">
                      <thead>
                         <tr>
                            <th>ID</th>
                            <th>Tanggal</th>
                            <th>NIS</th>
                            <th>Siswa</th>
                            <th>Keterangan</th>
                         </tr>
                      </thead>
                      <tbody id="preview-data">
                      </tbody>
                   </table>
                </div>
                <!-- /.card-body -->
             </div>
             <!-- /.card -->
          </div>
          <!-- /.col -->
       </div>
       <!-- /.row -->
    </section>
    <!-- /.content -->
 </div>
 <!-- /.content-wrapper -->

 <!-- Control Sidebar -->
 <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
 </aside>
 <!-- /.control-sidebar -->
 @include('frame.menu._FooterMenu')
 @include('script.trans.transkehadiran._transkehadiransiswa_script')
