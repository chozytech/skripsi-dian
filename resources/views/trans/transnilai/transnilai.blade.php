@include('frame._Header')
@include('frame.menu._HeaderMenu')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">

            <!-- Modal -->
            <div class="modal fade" id="crudModal" role="dialog" aria-labelledby="crudModalLabel" data-backdrop="static"
                data-keyboard="false" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="crudLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="formData" class="form-horizontal">
                                <div class="row" style="display:none;">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ID</label>
                                            <input type="text" disabled class="form-control" id="id" name="ID">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Action</label>
                                            <input type="text" disabled class="form-control" id="action" name="Action">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Kelas</label>
                                            <select class="form-control" id="kelas" name="Kelas" required></select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Siswa</label>
                                            <select class="form-control" id="siswa" name="Siswa" required></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" id="keterangan" name="Keterangan"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <table id="tbdatadetail" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Aksi</th>
                                        <th>Kode</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Nilai</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody id="preview-data">
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btSubmit" name="btSubmit"
                                class="btn btn-round btn-primary">Simpan</button>
                            <button type="button" class="btn btn-round btn-danger" id="btBatal" data-dismiss="modal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="crudDetailModal" role="dialog" aria-labelledby="crudDetailModalLabel"
                data-backdrop="static" data-keyboard="false" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="crudDetailLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="formDataDetail" class="form-horizontal">
                                <div class="row" style="display:none;">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ID</label>
                                            <input type="text" disabled class="form-control" id="id_detail" name="ID">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Action</label>
                                            <input type="text" disabled class="form-control" id="action_detail"
                                                name="Action">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mata Pelajaran</label>
                                            <select class="form-control" id="mata_pelajaran" name="Mata Pelajaran"
                                                required></select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nilai</label>
                                            <input type="number" min="0" class="form-control" id="nilai" name="Nilai"
                                                required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" id="keterangan_detail"
                                                name="Keterangan"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btSubmitDetail" name="btSubmitDetail"
                                class="btn btn-round btn-primary">Simpan</button>
                            <button type="button" class="btn btn-round btn-danger" data-dismiss="modal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ Session::get('page') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ Session::get('page') }} </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <input id="session_level" value="{{ Session::get('level_id') }}" hidden />

                        <table id="tbdata" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Aksi</th>
                                    <th>Siswa</th>
                                    <th>Kelas</th>
                                    <th>Tingkat</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Semester</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody id="preview-data">
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
@include('frame.menu._FooterMenu')
@include('script.trans.transnilai._transnilai_script')
