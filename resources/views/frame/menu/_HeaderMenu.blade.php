<body class="sidebar-mini layout-navbar-fixed layout-fixed layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-user-circle"></i> {{Session::get('username')}}
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item change-password">
                            <i class="fas fa-lock"></i> &nbsp;&nbsp;Ganti Password

                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ url('/logout') }}" class="dropdown-item">
                            <i class="fas fa-power-off "></i> &nbsp;&nbsp;Logout

                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ url('/') }}" class="brand-link">
                <img src="{{asset ('assets/img/logo.png')}}" alt="Company Logo" class="brand-image" style="opacity: .8">
                <span class="brand-text font-weight-light">SDN PEGALANGAN</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{asset ('assets/img/avatar_default.png')}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                    <a href="{{ url('/') }}" class="d-block">{{Session::get('username')}}</a>
                    </div>
                </div>
                @if(Session::get('level_id') ==3)      
                <select class="form-control" id="list_siswa" name="List Siswa"></select>
                @endif
                <!-- Sidebar Menu -->
               
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-header">Menu</li>
                        <!--<li class="nav-item">
                            <a id="menuDashboard" href="{{ url('/') }}" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                     Dashboard
                                </p>
                            </a>
                        </li>-->
                        @if(Session::get('level_id') ==1 || Session::get('level_id') ==2)       
                        <li class="nav-header">Master</li>
                        <li id="menuMaster" class="nav-item has-treeview">
                            <a id="menuMasterParent" href="#" class="nav-link">
                                <i class="nav-icon fas fa-database"></i>
                                <p>
                                    Master
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @if(Session::get('level_id') ==1)        
                                <li class="nav-item">
                                    <a id="menuMasterUser" href="{{ url('/masteruser') }}" class="nav-link">
                                        <i class="fas fa-user-friends nav-icon"></i>
                                        <p>Master User</p>
                                    </a>
                                </li>
                                @endif
                                <li class="nav-item">
                                    <a id="menuMasterGuru" href="{{ url('/masterguru') }}" class="nav-link">
                                        <i class="fas fa-user-nurse nav-icon"></i>
                                        <p>Master Guru</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuMasterSiswa" href="{{ url('/mastersiswa') }}" class="nav-link">
                                        <i class="fas fa-user-graduate nav-icon"></i>
                                        <p>Master Siswa</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuMasterWaliMurid" href="{{ url('/masterwalimurid') }}" class="nav-link">
                                        <i class="fas fa-user-tie nav-icon"></i>
                                        <p>Master Wali Murid</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuMasterKelas" href="{{ url('/masterkelas') }}" class="nav-link">
                                        <i class="fab fa-flipboard nav-icon"></i>
                                        <p>Master Kelas</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuMasterMataPelajaran" href="{{ url('/mastermatapelajaran') }}" class="nav-link">
                                        <i class="fas fa-book-reader nav-icon"></i>
                                        <p>Master Mata Pelajaran</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        @if(Session::get('level_id') ==1 || Session::get('level_id') ==2)    
                        <li class="nav-header">Transaksi</li>
                        <li id="menuTrans" class="nav-item has-treeview">
                            <a id="menuTransParent" href="#" class="nav-link">
                                <i class="nav-icon fas fa-download"></i>
                                <p>
                                    Transaksi
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a id="menuTransJadwal" href="{{ url('/transjadwal') }}" class="nav-link">
                                        <i class="fas fa-calendar nav-icon"></i>
                                        <p>Jadwal</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransJurnal" href="{{ url('/transjurnal') }}" class="nav-link">
                                        <i class="far fa-sticky-note nav-icon"></i>
                                        <p>Jurnal</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransKehadiran" href="{{ url('/transkehadiran') }}" class="nav-link">
                                        <i class="far fa-calendar-check nav-icon"></i>
                                        <p>Kehadiran</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransNilai" href="{{ url('/transnilai') }}" class="nav-link">
                                        <i class="fas fa-book nav-icon"></i>
                                        <p>Nilai</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransPengumuman" href="{{ url('/transpengumuman') }}" class="nav-link">
                                        <i class="fas fa-bell nav-icon"></i>
                                        <p>Pengumuman</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        @if(Session::get('level_id') ==3)    
                        <li class="nav-header">Transaksi</li>
                        <li id="menuTrans" class="nav-item has-treeview">
                            <a id="menuTransParent" href="#" class="nav-link">
                                <i class="nav-icon fas fa-download"></i>
                                <p>
                                    Transaksi
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a id="menuTransJadwal" href="{{ url('/transjadwal') }}" class="nav-link">
                                        <i class="fas fa-calendar nav-icon"></i>
                                        <p>Jadwal</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransJurnal" href="{{ url('/transjurnal') }}" class="nav-link">
                                        <i class="far fa-sticky-note nav-icon"></i>
                                        <p>Jurnal</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransKehadiran" href="{{ url('/transkehadiran') }}" class="nav-link">
                                        <i class="far fa-calendar-check nav-icon"></i>
                                        <p>Kehadiran</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransNilai" href="{{ url('/transnilai') }}" class="nav-link">
                                        <i class="fas fa-book nav-icon"></i>
                                        <p>Nilai</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="menuTransPengumuman" href="{{ url('/transpengumuman') }}" class="nav-link">
                                        <i class="fas fa-bell nav-icon"></i>
                                        <p>Pengumuman</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <!--<li class="nav-header">Laporan</li>
                        <li id="menuReport" class="nav-item has-treeview">
                            <a id="menuReportParent" href="#" class="nav-link">
                                <i class="nav-icon fas fa-newspaper"></i>
                                <p>
                                    Laporan
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                            </ul>
                        </li>-->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <input id="messages" name="messages" type="hidden" value="<?=  Session::get('alert_types') . ";" . Session::get('alert_messages') ?>" />
        <?php
            Session::put('alert', false);
            Session::put('alert_types', null);
            Session::put('alert_messages', null); ?>
        ?>
        <!-- Modal -->
        <div class="modal fade" id="changePasswordModal" role="dialog" aria-labelledby="changePasswordModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="changePasswordLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="formChangePassword" class="form-horizontal">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Password Lama</label>
                                        <input type="password" class="form-control" id="password_lama" name="Password Lama" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Password Baru</label>
                                        <input type="password" class="form-control" id="password_baru" name="Password Baru" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Konfirmasi Password Baru</label>
                                        <input type="password" class="form-control" id="confirm_password_baru" name="Konfirmasi Password Baru" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btSubmitChangePassword" name="btSubmitChangePassword" class="btn btn-round btn-primary">Simpan</button>
                        <button type="button" class="btn btn-round btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
