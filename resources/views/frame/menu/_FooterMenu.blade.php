<footer class="main-footer">
	<strong>Copyright &copy; <?= date('Y'); ?> <a href="{{ url('/') }}">Dian Ardiansyah</a>.</strong> All rights
	reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset ('assets/plugins/jquery/jquery.min.js')}} "></script>
<!-- Bootstrap -->
<script src="{{asset ('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}} "></script>
<!-- jQuery UI -->
<script src="{{asset ('assets/plugins/jquery-ui/jquery-ui.min.js')}} "></script>
<!-- Moment -->
<script src="{{asset ('assets/plugins/moment/moment.min.js')}} "></script>
<!-- Ekko Lightbox -->
<script src="{{asset ('assets/plugins/ekko-lightbox/ekko-lightbox.min.js')}} "></script>

<!-- ChartJS -->
<script src="{{asset ('assets/plugins/chart.js/Chart.min.js')}} "></script>
<!-- Sparkline -->
<script src="{{asset ('assets/plugins/sparklines/sparkline.js')}} "></script>
<!-- JQVMap -->
<script src="{{asset ('assets/plugins/jqvmap/jquery.vmap.min.js')}} "></script>
<script src="{{asset ('assets/plugins/jqvmap/maps/jquery.vmap.usa.js')}} "></script>
<!-- jQuery Knob Chart -->
<script src="{{asset ('assets/plugins/jquery-knob/jquery.knob.min.js')}} "></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset ('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}} "></script>
<!-- Summernote -->
<script src="{{asset ('assets/plugins/summernote/summernote-bs4.min.js')}} "></script>

<!-- DataTables -->
<script src="{{asset ('assets/plugins/datatables/jquery.dataTables.js')}} "></script>
<script src="{{asset ('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}} "></script>
<script src="{{asset ('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')}} "></script>
<script src="{{asset ('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}} "></script>
<script src="{{asset ('assets/plugins/datatables-buttons/js/buttons.flash.min.js')}} "></script>
<script src="{{asset ('assets/plugins/datatables-buttons/js/buttons.html5.min.js')}} "></script>
<script src="{{asset ('assets/plugins/datatables-buttons/js/buttons.print.min.js')}} "></script>
<!-- SweetAlert -->
<script src="{{asset ('assets/plugins/sweetalert/sweetalert2.all.js')}} "></script>
<!-- Select2 -->
<script src="{{asset ('assets/plugins/select2/js/select2.full.min.js')}} "></script>
<!-- Toastr -->
<script src="{{asset ('assets/plugins/toastr/toastr.min.js')}} "></script>
<!-- AdminLTE App -->
<script src="{{asset ('assets/js/adminlte.min.js')}} "></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset ('assets/js/demo.js')}} "></script>
<?php if (Session::get('page')  != "Beranda" && Session::get('page')  != "Jadwal Pelajaran" && Session::get('page')  != "Jurnal"){ ?>
    <!-- Bootstrap Date Time Picker -->
  <script src="{{asset ('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}} "></script>
  <?php } ?>
<!-- Filterizr-->
<script src="{{asset ('assets/plugins/filterizr/jquery.filterizr.min.js')}} "></script>
<!-- date-range-picker -->
<script src="{{asset ('assets/plugins/daterangepicker/daterangepicker.js')}} "></script>
<!-- overlayScrollbars -->
<script src="{{asset ('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}} "></script>
<!-- Page specific script -->
<script>
	var is_trigger = true;
	$(document).ready(function() {
		$('#list_siswa').select2({
            placeholder: "Pilih Siswa"
        });
		GetListSiswaByWaliMuridCode('{{Session::get('username')}}');
		var message = $("#messages").val();
		var messagearr = message.split(";")
		if (message != ";") {
			if (messagearr[0] == "Success") {
				toastr.success(messagearr[1]);
			} else if (messagearr[0] == "Error") {
				toastr.error(messagearr[1]);
			} else if (messagearr[0] == "Warning") {
				toastr.warning(messagearr[1]);
			} else if (messagearr[0] == "Info") {
				toastr.info(messagearr[1]);
			}
		}

		$('.modal').on('shown.bs.modal', function() {
			var height = $(this).find('.modal-dialog').outerHeight();
			if (height > 450) {
				$(this).find(".modal-body").css('height', 450);
				$(this).find(".modal-body").scrollTop(0);
			}
			$(this).modal({
				backdrop: 'static',
				keyboard: false
			})
		});
		$('.change-password').click(function(e) {
			event.preventDefault();
			$("#changePasswordLabel").text("Ganti Password");
			$('#changePasswordModal').modal('show');
			$('#password_lama').val("");
			$('#password_baru').val("");
			$('#confirm_password_baru').val("");
		});

		$('#btSubmitChangePassword').click(function(e) {
			event.preventDefault();
			$("#formChangePassword").submit();
		});

		$("#formChangePassword").submit(function(e) {
			e.preventDefault();
			if (CheckRequiredChangePassword()) {
				if (CheckValidateChangePassword()) {
					$.ajax({
						type: 'POST',
						url: "{{ url('/') }}" + "ChangePassword/Save",
						data: {
							data: $("#formChangePassword").serialize()
						},
						success: function(response) {
							if (response.status) {
								$('#changePasswordModal').modal('hide');
								swal("Berhasil diubah !", response.messages, "success", );
							} else {
								swal("Gagal !", response.messages, "error", );
							}
						}
					});
				}
			}
		});

		$('#list_siswa').on("change", function(e) {
			if (is_trigger){
            	window.location = "{{ url('/') }}" + "/changesiswa/switchsiswa?nis=" + this.value;
			}
			is_trigger = true;
        });
	});

	function GetListSiswaByWaliMuridCode(code) {
        $('#list_siswa').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('selectlist/GetListSiswaByWaliMuridCode') }}",
            data: {
                _token: "{{ csrf_token() }}",
                Code: code
            },
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#list_siswa").append(newOption);
                }
				is_trigger = false;
				if ($("#list_siswa").val() != undefined){
					$("#list_siswa").val({{Session::get('siswa_nis')}}).trigger('change');
				}
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Siswa');
            }
        });

    }

	function CheckRequiredChangePassword() {
		var fields = document.getElementById("formChangePassword").querySelectorAll("[required]");
		var result = true;
		$.each(fields, function(i, field) {
			if (!field.value) {
				toastr.error(field.name.charAt(0).toUpperCase() + field.name.slice(1) + ' tidak boleh kosong');
				result = false;
			}
		});
		return result;
	}

	function CheckValidateChangePassword() {

		var result = true;
		var newpw = $("#password_baru").val();
		var renewpw = $("#confirm_password_baru").val();

		if (newpw != renewpw) {
			swal("Gagal !", "Password baru dan konfirmasi password baru tidak sama." , "error", );
			result = false;
		}
		return result;
	}
</script>
