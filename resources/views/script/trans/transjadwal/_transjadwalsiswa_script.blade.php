<script>
    var rt = "";
    $(document).ready(function() {
        $("#menuTrans").addClass("menu-open").find(">.nav-link").slideDown(500);
        $("#menuTransParent").addClass("active");
        $("#menuTransJadwal").addClass("active");

        // Setup - add a text input to each footer cell
        $('#tbdata thead tr').clone(true).appendTo('#tbdata thead');
        $('#tbdata thead tr:eq(1) th').each(function(i) {

            if (i > 1) {
                var title = $(this).text();
                $(this).html(
                    '<input type="text" class="form-control form-control-sm" placeholder="Cari ' +
                    title + '" />');

                $('input', this).on('keyup change', function() {
                    if (tbdata.column(i).search() !== this.value) {
                        tbdata
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                var title = $(this).text();
                $(this).html('');
            }
        });
    
        var tbdata = $("#tbdata").DataTable({
            dom: "Bfrtip",
            orderCellsTop: true,
            fixedHeader: true,
            buttons: [],
            ajax: {
                "url": window.location.href + "/GetListData",
                "type": "POST",
                "data": function(d) {
                    d._token = "{{ csrf_token() }}";
                }
            },
            "columnDefs": [{
                    "data": "ID",
                    "visible": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "data": "hari",
                    "targets": 1,
                    "width": "20%"
                },
                {
                    "data": "jam",
                    "targets": 2,
                    "width": "20%"
                },
                {
                    "data": "mata_pelajaran_code",
                    "targets": 3,
                    "width": "20%"
                },
                {
                    "data": "mata_pelajaran_name",
                    "targets": 4,
                    "width": "20%"
                },
                {
                    "data": "keterangan",
                    "targets": 5,
                    "width": "30%"
                },
            ],
            "scrollX": true,
            "order": [],
            "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()
                        ._iDisplayLength) > 1) {
                    $(".dataTables_paginate").css("display", "block");
                } else {
                    $(".dataTables_paginate").css("display", "none");
                }

                if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                  
                } else {
                   
                }
            }
        });        

        $(".decimal").keypress(function(event) {
            return isNumberKey(event);
        });
    });
</script>
</body>

</html>
