<script>
    var rt = "";
    $(document).ready(function() {
        $("#menuTrans").addClass("menu-open").find(">.nav-link").slideDown(500);
        $("#menuTransParent").addClass("active");
        $("#menuTransKehadiran").addClass("active");

        $('#siswa').select2({
            placeholder: "Pilih Siswa"
        });
        $('#kelas').select2({
            placeholder: "Pilih Kelas"
        });
        $('#date').daterangepicker({
            locale: {
                format: 'DD/MM/YYYY',
            },  
            singleDatePicker: true,
        });
        
        GetListKelas();

        GetListSiswaByKelasCode($('#kelas').val());

        // Setup - add a text input to each footer cell
        $('#tbdata thead tr').clone(true).appendTo('#tbdata thead');
        $('#tbdata thead tr:eq(1) th').each(function(i) {

            if (i > 1) {
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Cari ' + title + '" />');

                $('input', this).on('keyup change', function() {
                    if (tbdata.column(i).search() !== this.value) {
                        tbdata
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                var title = $(this).text();
                $(this).html('');
            }
        });
        $('#tbdatadetail thead tr').clone(true).appendTo('#tbdatadetail thead');
        $('#tbdatadetail thead tr:eq(1) th').each(function(i) {

            if (i > 1) {
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Cari ' + title + '" />');

                $('input', this).on('keyup change', function() {
                    if (tbdatadetail.column(i).search() !== this.value) {
                        tbdatadetail
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                var title = $(this).text();
                $(this).html('');
            }
        });
        var tbdata = $("#tbdata").DataTable({
            dom: "Bfrtip",
            orderCellsTop: true,
            fixedHeader: true,
            buttons: [{
                text: "+ Tambah data baru",
                action: function(e, dt, node, config) {
                    rt = "";
                    $("#crudLabel").text("Tambah Data");
                    $("#action").val("add");
                    $('#crudModal').modal('show');
                    InitElement("add");
                    $('#tbdatadetail').DataTable().clear().draw();
                },
                className: "btn-sm btn-default btn-new newdata"
            }],
            ajax: {
                "url": window.location.href + "/GetListData",
                "type": "POST",
                "data": function(d) {
                    d._token =  "{{ csrf_token() }}";
                }
            },
            "columnDefs": [{
                    "data": "ID",
                    "visible": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "data": null,
                    "orderable": false,
                    "width": "10%",
                    "render": function(data, type, full, meta) {
                        var actionbutton = "";
                        actionbutton = "<center>";

                            if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                            actionbutton += "<a href=\"#\" id=\"btedit\"><i class=\"fas fa-edit\" rel=\"tooltip\" title=\"Sunting\"></i> </a>&nbsp;&nbsp;";
                            actionbutton += "<a href=\"#\" id=\"btdelete\"><i class=\"fas fa-trash\" rel=\"tooltip\" title=\"Hapus\"></i> </a>&nbsp;&nbsp;";
                        }
                        return actionbutton;
                    },
                    "targets": 1
                },
                {
                    "data": "tanggal",
                    "targets": 2,
                    "width": "20%"
                },
                {
                    "data": "kelas_name",
                    "targets": 3,
                    "width": "20%"
                },
                {
                    "data": "kelas_tingkat",
                    "targets": 4,
                    "width": "20%"
                },
                {
                    "data": "kelas_tahun_ajaran",
                    "targets": 5,
                    "width": "20%"
                },
                {
                    "data": "kelas_semester",
                    "targets": 6,
                    "width": "20%"
                },
                {
                    "data": "keterangan",
                    "targets": 7,
                    "width": "30%"
                },
            ],
            "scrollX": true,
            "order": [],
            "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $(".dataTables_paginate").css("display", "block");
                } else {
                    $(".dataTables_paginate").css("display", "none");
                }

                if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                  
                } else {
                    $(".newdata").hide();
                }
            }
        });

        var tbdatadetail = $("#tbdatadetail").DataTable({
            dom: "Bfrtip",
            orderCellsTop: true,
            fixedHeader: true,
            buttons: [{
                text: "+ Tambah data detail",
                action: function(e, dt, node, config) {
                    $("#crudDetailLabel").text("Tambah Data");

                    $('#crudDetailModal').modal('show');

                    InitElementDetail("add");
                    $("#action_detail").val("add");
                },
                className: "btn-sm btn-default btn-new newdata"
            }],
            "columnDefs": [{
                    "data": "ID",
                    "visible": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "data": null,
                    "orderable": false,
                    "width": "10%",
                    "render": function(data, type, full, meta) {
                        var actionbutton = "";
                        actionbutton = "<center>";
                        actionbutton += "<a href=\"#\" id=\"btedit\"><i class=\"fas fa-edit\" rel=\"tooltip\" title=\"Sunting\"></i> </a>&nbsp;&nbsp;";
                        if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                            actionbutton += "<a href=\"#\" id=\"btdelete\"><i class=\"fas fa-trash\" rel=\"tooltip\" title=\"Hapus\"></i> </a>&nbsp;&nbsp;";
                        }
                        return actionbutton;
                    },
                    "targets": 1
                },
                {
                    "data": "siswa_nis",
                    "targets": 2,
                    "width": "20%"
                },
                {
                    "data": "siswa_name",
                    "targets": 3,
                    "width": "20%"
                },
                {
                    "data": "keterangan",
                    "targets": 4,
                    "width": "30%"
                },
            ],
            "order": [],
            "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $(".dataTables_paginate").css("display", "block");
                } else {
                    $(".dataTables_paginate").css("display", "none");
                }

                if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {

                } else {

                }
            }
        });

        $('#btSubmit').click(function(e) {
            event.preventDefault();
            $("#formData").submit();
        });

        $('#btSubmitDetail').click(function(e) {
            if (CheckRequiredDetail()) {
                var isExistDetail = false;
                var currentRow = $('#tbdatadetail').dataTable().fnGetData().length;
                var mode = $("#action_detail").val();
                try {
                    tbdatadetail.rows(function(idx, data, node) {
                        if (mode.toUpperCase() == "ADD") {
                            if (data.siswa_nis.toUpperCase() == $("#siswa").val()) {
                                isExistDetail = true;
                            }
                        }
                    });
                    if (!isExistDetail) {
                        var dataToAdd = {
                            "ID": (mode.toUpperCase() == "ADD" ? ('TEMP_' + (currentRow + 1)) : $("#id_detail").val()),
                            "siswa_nis": $("#siswa").val(),
                            "siswa_name": $("#siswa").select2('data')[0].text,
                            "keterangan": $("#keterangan_detail").val()
                        };
                        if (mode.toUpperCase() == "ADD") {
                            tbdatadetail.row.add(dataToAdd).draw(true);
                        } else if (mode.toUpperCase() == "EDIT") {
                            var total = 0;
                            tbdatadetail.rows(function(idx, data, node) {
                                if (data.ID == $("#id_detail").val()) {
                                    selectedIndex = idx;
                                }

                            });
                            tbdatadetail.row(selectedIndex).data(dataToAdd).draw(true);
                        }

                        $('#crudDetailModal').modal('hide');
                        swal("Berhasil disimpan !", "Data detail berhasil disimpan.", "success", );

                    } else {
                        swal("Gagal !", "Detail sudah ada dalam undang undang.", "error", );
                    }
                } catch (err) {
                    swal("Gagal !", err.message, "error", );
                }
            }

        });

        $("#formData").submit(function(e) {
            e.preventDefault();
            if (CheckRequired()) {
                if (CheckValidate()) {
                    var idData = ($("#id").val() != "" && $("#id").val() != undefined) ? $("#id").val() : null;
                    $.ajax({
                        type: 'POST',
                        url: window.location.href + "/Save",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: idData,
                            data: $("#formData").serialize(),
                            detail: JSON.stringify(tbdatadetail.data().toArray())
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                $('#crudModal').modal('hide');
                                swal("Berhasil disimpan !", response.messages, "success", ).then(function() {
                                    location.reload();
                                });
                            } else {
                                swal("Gagal !", response.messages, "error", ).then(function() {
                                    location.reload();
                                });
                            }
                        }
                    });
                }
            }
        });

        $("#tbdata tbody").on("click", "#btedit", function(event) {
            event.preventDefault();
            $("#crudLabel").text("Edit Data");
            $("#action").val("edit");
            InitElement("edit");
            var data = tbdata.row($(this).parents('tr')).data();
            $.ajax({
                url: window.location.href + "/Edit",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: data.ID
                },
                dataType: 'json',
                success: function(response) {
                    $("#id").val(response.ID);
                    $("#tanggal").val(response.Tanggal);
                    GetListDetailByKehadiranId(response.ID);
                    $("#kelas").val(response.Kelas_Code).trigger("change");
                    $("#keterangan").val(response.Keterangan);
                    $("#crudModal").modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal(
                        "Gagal !",
                        textStatus + " : " + errorThrown,
                        "error"
                    ).then(function() {
                        location.reload();
                    });
                }
            });

        });


        $("#tbdatadetail tbody").on("click", "#btedit", function(event) {
            event.preventDefault();
            $("#crudDetailLabel").text("Edit Data");
            InitElementDetail("edit");
            $("#action_detail").val("edit");
            var data = tbdatadetail.row($(this).parents('tr')).data();
            $("#id_detail").val(data.ID);
            $("#siswa").val(data.siswa_nis).trigger("change");
            $("#keterangan_detail").val(data.keterangan);
            $("#crudDetailModal").modal('show');
        });


        $("#tbdata tbody").on("click", "#btdelete", function(event) {
            event.preventDefault();
            var data = tbdata.row($(this).parents('tr')).data();
            swal({
                title: "Hapus data ?",
                text: "Anda tidak akan dapat mengembalikan ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
            }).then(function(inputValue) {
                if (inputValue.value) {
                    $.ajax({
                        url: window.location.href + "/Delete",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: data.ID
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                swal(
                                    "Terhapus",
                                    "Data berhasil dihapus.",
                                    "success"
                                ).then(function() {
                                    location.reload();
                                });
                            } else {
                                swal(
                                    "Gagal !",
                                    response.messages,
                                    "error"
                                ).then(function() {
                                    location.reload();
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal(
                                "Gagal !",
                                textStatus + " : " + errorThrown,
                                "error"
                            ).then(function() {
                                location.reload();
                            });
                        }
                    });
                }
            })
        });

        $("#tbdatadetail tbody").on("click", "#btdelete", function(event) {
            event.preventDefault();
            var data = tbdatadetail.row($(this).parents('tr')).data();
            var selectedData = this;
            if (data.ID.indexOf("TEMP".toUpperCase()) != -1 || data.ID == "") {
                swal({
                    title: "Hapus data ?",
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger m-l-10",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak"
                }).then(function(inputValue) {
                    if (inputValue.value) {
                        tbdatadetail.rows(function(idx, dt, node) {
                            if (dt.ID == data.ID) {
                                selectedIndex = idx;
                            }
                        });
                        tbdatadetail.row($(selectedData).parents('tr')).remove().draw();
                        swal(
                            "Terhapus",
                            "Data berhasil dihapus.",
                            "success"
                        );
                    }
                });

            } else {

                swal({
                    title: "Hapus data ?",
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger m-l-10",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak"
                }).then(function(inputValue) {
                    if (inputValue.value) {
                        $.ajax({
                            url: window.location.href + "/DeleteDetail",
                            type: "POST",
                            data: {
                                _token: "{{ csrf_token() }}",
                                id: data.ID
                            },
                            dataType: 'json',
                            success: function(response) {
                                if (response.status) {
                                    tbdatadetail.rows(function(idx, dt, node) {
                                        if (dt.ID == data.ID) {
                                            selectedIndex = idx;
                                        }

                                    });
                                    tbdatadetail.row($(selectedData).parents('tr')).remove().draw();
                                    swal(
                                        "Terhapus",
                                        "Data berhasil dihapus.",
                                        "success"
                                    );
                                } else {
                                    swal(
                                        "Gagal !",
                                        response.messages,
                                        "error"
                                    );
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                swal(
                                    "Gagal !",
                                    textStatus + " : " + errorThrown,
                                    "error"
                                ).then(function() {
                                    location.reload();
                                });
                            }
                        });
                    }
                })
            }
        });

        $(".decimal").keypress(function(event) {
            return isNumberKey(event);
        });

        $('#kelas').on("change", function(e) {
            GetListSiswaByKelasCode(this.value);
        });
    });

    function CheckRequired() {
        var fields = document.getElementById("formData").querySelectorAll("[required]");
        var result = true;
        $.each(fields, function(i, field) {
            if (!field.value) {
                toastr.error(field.name.charAt(0).toUpperCase() + field.name.slice(1) + ' tidak boleh kosong');
                result = false;
            }
        });
        return result;
    }

    function CheckRequiredDetail() {
        var fields = document.getElementById("formDataDetail").querySelectorAll("[required]");
        var result = true;
        $.each(fields, function(i, field) {
            if (!field.value) {
                toastr.error(field.name.charAt(0).toUpperCase() + field.name.slice(1) + ' tidak boleh kosong');
                result = false;
            }
        });
        return result;
    }

    function InitElement(action) {
        if (action == "add") {
            $("#id").val("");
            $("#formData").each(function() {
                $(this).find('.form-control').val("");
            });
            $("#kelas").val("").trigger("change");
            $("#code").prop('disabled', false);
            $('#date').data('daterangepicker').setStartDate(new Date());
        } else if (action == "edit") {
            $("#code").prop('disabled', true);
        }
    }

    function InitElementDetail(action) {
        if (action == "add") {
            $("#id_detail").val("");
            $("#formDataDetail").each(function() {
                $(this).find('.form-control').val("");
            });

            $("#siswa").val("").trigger("change");
        } else if (action == "edit") {}
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [day, month, year].join('/');
    }

    function CheckValidate() {

        var message = "";
        var result = true;

        if (!result) {
            toastr.error(message);
            return false;
        } else {
            return true;
        }
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 &&
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function GetListDetailByKehadiranId(id) {
        $('#tbdatadetail').DataTable().clear().draw();
        $.ajax({
            type: 'POST',
            url: "{{ url('/transkehadiran/GetListDetailByKehadiranId') }}",
            data: {
                _token: "{{ csrf_token() }}",
                ID: id
            },
            dataType: 'json',
            success: function(response) {
                if (response.data != ""){
                    if (response.data.length > 0){
                        $('#tbdatadetail').DataTable().rows.add(response.data).draw();
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Struktur');
            }
        });

    }


    function GetListSiswaByKelasCode(kelas_code) {
        $('#siswa').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('selectlist/GetListSiswaByKelasCode') }}",
            data: {
                _token: "{{ csrf_token() }}",
                Code: kelas_code
            },
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#siswa").append(newOption);
                }
                $("#siswa").val("").trigger("change");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Detail');
            }
        });

    }

    function GetListKelas() {
        $('#kelas').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('selectlist/GetListKelas') }}",
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#kelas").append(newOption).trigger('change');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Kelas');
            }
        });

    }
</script>
</body>

</html>
