<script>
    $(document).ready(function() {
        $("#menuTrans").addClass("menu-open").find(">.nav-link").slideDown(500);
        $("#menuTransParent").addClass("active");
        $("#menuTransPengumuman").addClass("active");

        $('#kelas').select2({
            placeholder: "Pilih Kelas"
        });

        GetListKelas();

        $('#tbdata thead tr').clone(true).appendTo('#tbdata thead');
        $('#tbdata thead tr:eq(1) th').each(function(i) {

            if (i > 1) {
                var title = $(this).text();
                $(this).html(
                    '<input type="text" class="form-control form-control-sm" placeholder="Cari ' +
                    title + '" />');

                $('input', this).on('keyup change', function() {
                    if (tbdata.column(i).search() !== this.value) {
                        tbdata
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                var title = $(this).text();
                $(this).html('');
            }
        });
        var tbdata = $("#tbdata").DataTable({
            dom: "Bfrtip",
            orderCellsTop: true,
            fixedHeader: true,
            buttons: [{
                text: "+ Tambah data baru",
                action: function(e, dt, node, config) {
                    $("#crudLabel").text("Tambah Data");
                    $("#action").val("add");
                    $('#crudModal').modal('show');
                    InitElement("add");
                },
                className: "btn-sm btn-default btn-new newdata"
            }],
            ajax: {
                "url": window.location.href + "/GetListData",
                "type": "POST",
                "data": function(d) {
                    d._token = "{{ csrf_token() }}";
                }
            },
            "columnDefs": [{
                    "data": "ID",
                    "visible": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "data": null,
                    "orderable": false,
                    "width": "10%",
                    "render": function(data, type, full, meta) {
                        var actionbutton = "";
                        actionbutton = "<center>";

                            if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                            actionbutton +=
                                "<a href=\"#\" id=\"btedit\"><i class=\"fas fa-edit\" rel=\"tooltip\" title=\"Sunting\"></i> </a>&nbsp;&nbsp;";
                            actionbutton +=
                                "<a href=\"#\" id=\"btdelete\"><i class=\"fas fa-trash\" rel=\"tooltip\" title=\"Hapus\"></i> </a>&nbsp;&nbsp;";
                        }
                        return actionbutton;
                    },
                    "targets": 1
                },
                {
                    "data": "code",
                    "targets": 2,
                    "width": "20%"
                },
                {
                    "data": "name",
                    "targets": 3,
                    "width": "20%"
                },
                {
                    "data": null,
                    "orderable": false,
                    "width": "20%",
                    "render": function(data, type, full, meta) {
                        var actionbutton = "";
                        actionbutton = "<center>";

                        if (full.file != "") {
                            actionbutton +=
                                "<a href=\"{{asset ('')}}"+full.file+"\" target=\"_blank\" id=\"btview\"><i class=\"fas fa-eye\" rel=\"tooltip\" title=\"Lihat File Pengumuman\"></i> </a>&nbsp;&nbsp;";
                        }
                        return actionbutton;
                    },
                    "targets": 4
                },
                {
                    "data": "kelas_name",
                    "targets": 5,
                    "width": "20%"
                },
                {
                    "data": "kelas_tingkat",
                    "targets": 6,
                    "width": "20%"
                },
                {
                    "data": "kelas_tahun_ajaran",
                    "targets": 7,
                    "width": "20%"
                },
                {
                    "data": "kelas_semester",
                    "targets": 8,
                    "width": "20%"
                },
                {
                    "data": "keterangan",
                    "targets": 9,
                    "width": "30%"
                },
            ],
            "scrollX": true,
            "order": [],
            "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()
                        ._iDisplayLength) > 1) {
                    $(".dataTables_paginate").css("display", "block");
                } else {
                    $(".dataTables_paginate").css("display", "none");
                }

                if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                   
                } else {
                    $(".newdata").hide();
                }
            }
        });

        if ($("#session_level").val() == "3"){
            tbdata.columns( [1] ).visible( false );
        }

        $('#btSubmit').click(function(e) {
            event.preventDefault();
            $("#formData").submit();
        });

        $("#formData").submit(function(e) {
            e.preventDefault();
            if (CheckRequired()) {
                if (CheckValidate()) {
                    var idData = ($("#id").val() != "" && $("#id").val() != undefined) ? $("#id")
                    .val() : null;

                    var form_data = new FormData();
                    var file_data = null;
                    var background_data = null;
                    if ($("#file").val() != "") {
                        file_data = document.querySelector('#file').files[0];
                        form_data.append("File", file_data);
                    }
                    form_data.append('id', idData);
                    form_data.append('Kode', $("#code").val());
                    form_data.append('Nama', $("#name").val());
                    form_data.append('Kelas', $("#kelas").val());
                    form_data.append('Is_All', ($('#is_all').is(":checked") ? 1 : 0));
                    form_data.append('Keterangan', $("#keterangan").val());

                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: window.location.href + "/Save",
                        data: form_data,
                        contentType: false,
                        processData: false,
                        success: function(resp) {
                            var response = JSON.parse(resp);
                            if (response.status) {
                                $('#crudModal').modal('hide');
                                swal("Berhasil disimpan !", response.messages, "success", )
                                    .then(function() {
                                        location.reload();
                                    });
                            } else {
                                swal("Gagal !", response.messages, "error", ).then(
                                function() {
                                    location.reload();
                                });
                            }
                        }
                    });
                }
            }
        });

        $("#tbdata tbody").on("click", "#btedit", function(event) {
            event.preventDefault();
            $("#crudLabel").text("Edit Data");
            $("#action").val("edit");
            InitElement("edit");
            var data = tbdata.row($(this).parents('tr')).data();
            $.ajax({
                url: window.location.href + "/Edit",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: data.ID
                },
                dataType: 'json',
                success: function(response) {
                    $("#id").val(response.ID);
                    $("#code").val(response.Code);
                    $("#name").val(response.Name);
                    $("#kelas").val(response.Kelas_Code).trigger("change");
                    $("#file_url").val(response.File);
                    if (response.Is_All == 1) {
                        $("#is_all").prop('checked', true);
                    } else {
                        $("#is_all").prop('checked', false);
                    }
                    $("#keterangan").val(response.Keterangan);
                    $("#crudModal").modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal(
                        "Gagal !",
                        textStatus + " : " + errorThrown,
                        "error"
                    ).then(function() {
                        location.reload();
                    });
                }
            });

        });


        $("#tbdata tbody").on("click", "#btdelete", function(event) {
            event.preventDefault();
            var data = tbdata.row($(this).parents('tr')).data();
            swal({
                title: "Hapus data ?",
                text: "Anda tidak akan dapat mengembalikan ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
            }).then(function(inputValue) {
                if (inputValue.value) {
                    $.ajax({
                        url: window.location.href + "/Delete",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: data.ID
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                swal(
                                    "Terhapus",
                                    "Data berhasil dihapus.",
                                    "success"
                                ).then(function() {
                                    location.reload();
                                });
                            } else {
                                swal(
                                    "Gagal !",
                                    response.messages,
                                    "error"
                                ).then(function() {
                                    location.reload();
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal(
                                "Gagal !",
                                textStatus + " : " + errorThrown,
                                "error"
                            ).then(function() {
                                location.reload();
                            });
                        }
                    });
                }
            })
        });
    });

    function CheckRequired() {
        var fields = document.getElementById("formData").querySelectorAll("[required]");
        var result = true;
        $.each(fields, function(i, field) {
            if (!field.value) {
                toastr.error(field.name.charAt(0).toUpperCase() + field.name.slice(1) + ' tidak boleh kosong');
                result = false;
            }
        });
        return result;
    }

    function InitElement(action) {
        if (action == "add") {
            $("#id").val("");
            $("#file").val("");
            $("#formData").each(function() {
                $(this).find('.form-control').val("");
            });
            $("#kelas").val("").trigger("change");
            $("#is_all").prop('checked', false);
            $("#code").prop('disabled', false);
        } else if (action == "edit") {
            $("#file").val("");
            $("#code").prop('disabled', true);
        }
    }

    function CheckValidate() {

        var message = "";
        var result = true;

        if (!result) {
            toastr.error(message);
            return false;
        } else {
            return true;
        }
    }

    function GetListKelas() {
        $('#kelas').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('/selectlist/GetListKelas') }}",
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#kelas").append(newOption).trigger('change');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar kelas');
            }
        });

    }

</script>
</body>

</html>
