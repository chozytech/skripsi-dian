<script>
    var rt = "";
    $(document).ready(function() {
        $("#menuMaster").addClass("menu-open").find(">.nav-link").slideDown(500);
        $("#menuMasterParent").addClass("active");
        $("#menuMasterKelas").addClass("active");

        $('#siswa').select2({
            placeholder: "Pilih Siswa"
        });
        $('#tingkat').select2({
            placeholder: "Pilih Tingkat"
        });
        $('#semester').select2({
            placeholder: "Pilih Semester"
        });

        GetListSiswa();
        GetListTingkat();
        Semester();

        // Setup - add a text input to each footer cell
        $('#tbdata thead tr').clone(true).appendTo('#tbdata thead');
        $('#tbdata thead tr:eq(1) th').each(function(i) {

            if (i > 1) {
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Cari ' + title + '" />');

                $('input', this).on('keyup change', function() {
                    if (tbdata.column(i).search() !== this.value) {
                        tbdata
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                var title = $(this).text();
                $(this).html('');
            }
        });
        $('#tbdatasiswa thead tr').clone(true).appendTo('#tbdatasiswa thead');
        $('#tbdatasiswa thead tr:eq(1) th').each(function(i) {

            if (i > 1) {
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Cari ' + title + '" />');

                $('input', this).on('keyup change', function() {
                    if (tbdatasiswa.column(i).search() !== this.value) {
                        tbdatasiswa
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                var title = $(this).text();
                $(this).html('');
            }
        });
        var tbdata = $("#tbdata").DataTable({
            dom: "Bfrtip",
            orderCellsTop: true,
            fixedHeader: true,
            buttons: [{
                text: "+ Tambah data baru",
                action: function(e, dt, node, config) {
                    rt = "";
                    $("#crudLabel").text("Tambah Data");
                    $("#action").val("add");
                    $('#crudModal').modal('show');
                    InitElement("add");
                    $('#tbdatasiswa').DataTable().clear().draw();
                },
                className: "btn-sm btn-default btn-new newdata"
            }],
            ajax: {
                "url": window.location.href + "/GetListData",
                "type": "POST",
                "data": function(d) {
                    d._token =  "{{ csrf_token() }}";
                }
            },
            "columnDefs": [{
                    "data": "ID",
                    "visible": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "data": null,
                    "orderable": false,
                    "width": "10%",
                    "render": function(data, type, full, meta) {
                        var actionbutton = "";
                        actionbutton = "<center>";

                            if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                            actionbutton += "<a href=\"#\" id=\"btedit\"><i class=\"fas fa-edit\" rel=\"tooltip\" title=\"Sunting\"></i> </a>&nbsp;&nbsp;";
                            actionbutton += "<a href=\"#\" id=\"btdelete\"><i class=\"fas fa-trash\" rel=\"tooltip\" title=\"Hapus\"></i> </a>&nbsp;&nbsp;";
                        }
                        return actionbutton;
                    },
                    "targets": 1
                },
                {
                    "data": "code",
                    "targets": 2,
                    "width": "20%"
                },
                {
                    "data": "name",
                    "targets": 3,
                    "width": "20%"
                },
                {
                    "data": "tingkat",
                    "targets": 4,
                    "width": "20%"
                },
                {
                    "data": "tahun_ajaran",
                    "targets": 5,
                    "width": "20%"
                },
                {
                    "data": "semester",
                    "targets": 6,
                    "width": "20%"
                },
                {
                    "data": "keterangan",
                    "targets": 7,
                    "width": "30%"
                },
            ],
            "scrollX": true,
            "order": [],
            "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $(".dataTables_paginate").css("display", "block");
                } else {
                    $(".dataTables_paginate").css("display", "none");
                }

                if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                   
                } else {
                    $(".newdata").hide();
                }
            }
        });

        var tbdatasiswa = $("#tbdatasiswa").DataTable({
            dom: "Bfrtip",
            orderCellsTop: true,
            fixedHeader: true,
            buttons: [{
                text: "+ Tambah data siswa",
                action: function(e, dt, node, config) {
                    $("#crudSiswaLabel").text("Tambah Data");

                    $('#crudSiswaModal').modal('show');

                    InitElementSiswa("add");
                    $("#action_siswa").val("add");
                },
                className: "btn-sm btn-default btn-new newdata"
            }],
            "columnDefs": [{
                    "data": "ID",
                    "visible": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "data": null,
                    "orderable": false,
                    "width": "10%",
                    "render": function(data, type, full, meta) {
                        var actionbutton = "";
                        actionbutton = "<center>";
                        actionbutton += "<a href=\"#\" id=\"btedit\"><i class=\"fas fa-edit\" rel=\"tooltip\" title=\"Sunting\"></i> </a>&nbsp;&nbsp;";
                        if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                            actionbutton += "<a href=\"#\" id=\"btdelete\"><i class=\"fas fa-trash\" rel=\"tooltip\" title=\"Hapus\"></i> </a>&nbsp;&nbsp;";
                        }
                        return actionbutton;
                    },
                    "targets": 1
                },
                {
                    "data": "siswa_nis",
                    "targets": 2,
                    "width": "20%"
                },
                {
                    "data": "siswa_name",
                    "targets": 3,
                    "width": "20%"
                },
                {
                    "data": "keterangan",
                    "targets": 4,
                    "width": "30%"
                },
            ],
            "order": [],
            "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $(".dataTables_paginate").css("display", "block");
                } else {
                    $(".dataTables_paginate").css("display", "none");
                }

                if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {

                } else {

                }
            }
        });

        $('#btSubmit').click(function(e) {
            event.preventDefault();
            $("#formData").submit();
        });

        $('#btSubmitSiswa').click(function(e) {
            if (CheckRequiredSiswa()) {
                var isExistSiswa = false;
                var currentRow = $('#tbdatasiswa').dataTable().fnGetData().length;
                var mode = $("#action_siswa").val();
                try {
                    tbdatasiswa.rows(function(idx, data, node) {
                        if (mode.toUpperCase() == "ADD") {
                            if (data.siswa_nis.toUpperCase() == $("#siswa").val()) {
                                isExistSiswa = true;
                            }
                        }
                    });
                    if (!isExistSiswa) {
                        var dataToAdd = {
                            "ID": (mode.toUpperCase() == "ADD" ? ('TEMP_' + (currentRow + 1)) : $("#id_siswa").val()),
                            "siswa_nis": $("#siswa").val(),
                            "siswa_name": $("#siswa").select2('data')[0].text,
                            "keterangan": $("#keterangan_siswa").val()
                        };
                        if (mode.toUpperCase() == "ADD") {
                            tbdatasiswa.row.add(dataToAdd).draw(true);
                        } else if (mode.toUpperCase() == "EDIT") {
                            var total = 0;
                            tbdatasiswa.rows(function(idx, data, node) {
                                if (data.ID == $("#id_siswa").val()) {
                                    selectedIndex = idx;
                                }

                            });
                            tbdatasiswa.row(selectedIndex).data(dataToAdd).draw(true);
                        }

                        $('#crudSiswaModal').modal('hide');
                        swal("Berhasil disimpan !", "Data siswa berhasil disimpan.", "success", );

                    } else {
                        swal("Gagal !", "Siswa sudah ada dalam undang undang.", "error", );
                    }
                } catch (err) {
                    swal("Gagal !", err.message, "error", );
                }
            }

        });

        $("#formData").submit(function(e) {
            e.preventDefault();
            if (CheckRequired()) {
                if (CheckValidate()) {
                    var idData = ($("#id").val() != "" && $("#id").val() != undefined) ? $("#id").val() : null;
                    $.ajax({
                        type: 'POST',
                        url: window.location.href + "/Save",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: idData,
                            data: $("#formData").serialize(),
                            siswa: JSON.stringify(tbdatasiswa.data().toArray())
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                $('#crudModal').modal('hide');
                                swal("Berhasil disimpan !", response.messages, "success", ).then(function() {
                                    location.reload();
                                });
                            } else {
                                swal("Gagal !", response.messages, "error", ).then(function() {
                                    location.reload();
                                });
                            }
                        }
                    });
                }
            }
        });

        $("#tbdata tbody").on("click", "#btedit", function(event) {
            event.preventDefault();
            $("#crudLabel").text("Edit Data");
            $("#action").val("edit");
            InitElement("edit");
            var data = tbdata.row($(this).parents('tr')).data();
            $.ajax({
                url: window.location.href + "/Edit",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: data.ID
                },
                dataType: 'json',
                success: function(response) {
                    $("#id").val(response.ID);
                    $("#code").val(response.Code);
                    GetListSiswaByKelasId(response.ID);
                    $("#name").val(response.Name);
                    $("#tingkat").val(response.Tingkat).trigger("change");
                    $("#tahun_ajaran").val(response.Tahun_Ajaran);
                    $("#semester").val(response.Semester).trigger("change");
                    $("#keterangan").val(response.Keterangan);
                    $("#crudModal").modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal(
                        "Gagal !",
                        textStatus + " : " + errorThrown,
                        "error"
                    ).then(function() {
                        location.reload();
                    });
                }
            });

        });


        $("#tbdatasiswa tbody").on("click", "#btedit", function(event) {
            event.preventDefault();
            $("#crudSiswaLabel").text("Edit Data");
            InitElementSiswa("edit");
            $("#action_siswa").val("edit");
            var data = tbdatasiswa.row($(this).parents('tr')).data();
            $("#id_siswa").val(data.ID);
            $("#siswa").val(data.siswa_nis).trigger("change");
            $("#keterangan_siswa").val(data.keterangan);
            $("#crudSiswaModal").modal('show');
        });


        $("#tbdata tbody").on("click", "#btdelete", function(event) {
            event.preventDefault();
            var data = tbdata.row($(this).parents('tr')).data();
            swal({
                title: "Hapus data ?",
                text: "Anda tidak akan dapat mengembalikan ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
            }).then(function(inputValue) {
                if (inputValue.value) {
                    $.ajax({
                        url: window.location.href + "/Delete",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: data.ID
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                swal(
                                    "Terhapus",
                                    "Data berhasil dihapus.",
                                    "success"
                                ).then(function() {
                                    location.reload();
                                });
                            } else {
                                swal(
                                    "Gagal !",
                                    response.messages,
                                    "error"
                                ).then(function() {
                                    location.reload();
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal(
                                "Gagal !",
                                textStatus + " : " + errorThrown,
                                "error"
                            ).then(function() {
                                location.reload();
                            });
                        }
                    });
                }
            })
        });

        $("#tbdatasiswa tbody").on("click", "#btdelete", function(event) {
            event.preventDefault();
            var data = tbdatasiswa.row($(this).parents('tr')).data();
            var selectedData = this;
            if (data.ID.indexOf("TEMP".toUpperCase()) != -1 || data.ID == "") {
                swal({
                    title: "Hapus data ?",
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger m-l-10",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak"
                }).then(function(inputValue) {
                    if (inputValue.value) {
                        tbdatasiswa.rows(function(idx, dt, node) {
                            if (dt.ID == data.ID) {
                                selectedIndex = idx;
                            }
                        });
                        tbdatasiswa.row($(selectedData).parents('tr')).remove().draw();
                        swal(
                            "Terhapus",
                            "Data berhasil dihapus.",
                            "success"
                        );
                    }
                });

            } else {

                swal({
                    title: "Hapus data ?",
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger m-l-10",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak"
                }).then(function(inputValue) {
                    if (inputValue.value) {
                        $.ajax({
                            url: window.location.href + "/DeleteSiswa",
                            type: "POST",
                            data: {
                                _token: "{{ csrf_token() }}",
                                id: data.ID
                            },
                            dataType: 'json',
                            success: function(response) {
                                if (response.status) {
                                    tbdatasiswa.rows(function(idx, dt, node) {
                                        if (dt.ID == data.ID) {
                                            selectedIndex = idx;
                                        }

                                    });
                                    tbdatasiswa.row($(selectedData).parents('tr')).remove().draw();
                                    swal(
                                        "Terhapus",
                                        "Data berhasil dihapus.",
                                        "success"
                                    );
                                } else {
                                    swal(
                                        "Gagal !",
                                        response.messages,
                                        "error"
                                    );
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                swal(
                                    "Gagal !",
                                    textStatus + " : " + errorThrown,
                                    "error"
                                ).then(function() {
                                    location.reload();
                                });
                            }
                        });
                    }
                })
            }
        });

        $(".decimal").keypress(function(event) {
            return isNumberKey(event);
        });
    });

    function CheckRequired() {
        var fields = document.getElementById("formData").querySelectorAll("[required]");
        var result = true;
        $.each(fields, function(i, field) {
            if (!field.value) {
                toastr.error(field.name.charAt(0).toUpperCase() + field.name.slice(1) + ' tidak boleh kosong');
                result = false;
            }
        });
        return result;
    }

    function CheckRequiredSiswa() {
        var fields = document.getElementById("formDataSiswa").querySelectorAll("[required]");
        var result = true;
        $.each(fields, function(i, field) {
            if (!field.value) {
                toastr.error(field.name.charAt(0).toUpperCase() + field.name.slice(1) + ' tidak boleh kosong');
                result = false;
            }
        });
        return result;
    }

    function InitElement(action) {
        if (action == "add") {
            $("#id").val("");
            $("#formData").each(function() {
                $(this).find('.form-control').val("");
            });
            $("#tingkat").val("").trigger("change");
            $("#semester").val("").trigger("change");
            $("#code").prop('disabled', false);
        } else if (action == "edit") {
            $("#code").prop('disabled', true);
        }
    }

    function InitElementSiswa(action) {
        if (action == "add") {
            $("#id_siswa").val("");
            $("#formDataSiswa").each(function() {
                $(this).find('.form-control').val("");
            });

            $("#siswa").val("").trigger("change");
        } else if (action == "edit") {}
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [day, month, year].join('/');
    }

    function CheckValidate() {

        var message = "";
        var result = true;

        if (!result) {
            toastr.error(message);
            return false;
        } else {
            return true;
        }
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 &&
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function GetListSiswaByKelasId(id) {
        $('#tbdatasiswa').DataTable().clear().draw();
        $.ajax({
            type: 'POST',
            url: "{{ url('/masterkelas/GetListSiswaByKelasId') }}",
            data: {
                _token: "{{ csrf_token() }}",
                ID: id
            },
            dataType: 'json',
            success: function(response) {
                if (response.data != ""){
                    if (response.data.length > 0){
                        $('#tbdatasiswa').DataTable().rows.add(response.data).draw();
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Struktur');
            }
        });

    }


    function GetListSiswa() {
        $('#siswa').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('selectlist/GetListSiswa') }}",
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#siswa").append(newOption).trigger('change');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Siswa');
            }
        });

    }
    function GetListTingkat() {
        $('#tingkat').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('selectlist/GetListTingkat') }}",
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#tingkat").append(newOption).trigger('change');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Tingkat');
            }
        });

    }
    function Semester() {
        $('#semester').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('selectlist/GetListSemester') }}",
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#semester").append(newOption).trigger('change');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar Semester');
            }
        });

    }
</script>
</body>

</html>
