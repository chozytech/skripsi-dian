<script>
    $(document).ready(function() {
        $("#menuMaster").addClass("menu-open").find(">.nav-link").slideDown(500);
        $("#menuMasterParent").addClass("active");
        $("#menuMasterSiswa").addClass("active");

        $('#agama').select2({
            placeholder: "Pilih Agama"
        });

        $('#gender').select2({
            placeholder: "Pilih Jenis Kelamin"
        });

        $('#birth_date').daterangepicker({
            locale: {
                format: 'DD/MM/YYYY',
            },  
            singleDatePicker: true,
        });

        GetListAgama();
        GetListGender();

        $('#tbdata thead tr').clone(true).appendTo('#tbdata thead');
        $('#tbdata thead tr:eq(1) th').each(function(i) {

            if (i > 1) {
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Cari ' + title + '" />');

                $('input', this).on('keyup change', function() {
                    if (tbdata.column(i).search() !== this.value) {
                        tbdata
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                var title = $(this).text();
                $(this).html('');
            }
        });
        var tbdata = $("#tbdata").DataTable({
            dom: "Bfrtip",
            orderCellsTop: true,
            fixedHeader: true,
            buttons: [{
                text: "+ Tambah data baru",
                action: function(e, dt, node, config) {
                    $("#crudLabel").text("Tambah Data");
                    $("#action").val("add");
                    $('#crudModal').modal('show');
                    InitElement("add");
                },
                className: "btn-sm btn-default btn-new newdata"
            }],
            ajax: {
                "url": window.location.href + "/GetListData",
                "type": "POST",
                "data": function(d) {
                    d._token =  "{{ csrf_token() }}";
                }
            },
            "columnDefs": [{
                    "data": "ID",
                    "visible": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "data": null,
                    "orderable": false,
                    "width": "10%",
                    "render": function(data, type, full, meta) {
                        var actionbutton = "";
                        actionbutton = "<center>";

                            if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                            actionbutton += "<a href=\"#\" id=\"btedit\"><i class=\"fas fa-edit\" rel=\"tooltip\" title=\"Sunting\"></i> </a>&nbsp;&nbsp;";
                            actionbutton += "<a href=\"#\" id=\"btdelete\"><i class=\"fas fa-trash\" rel=\"tooltip\" title=\"Hapus\"></i> </a>&nbsp;&nbsp;";
                        }
                        return actionbutton;
                    },
                    "targets": 1
                },
                {
                    "data": "nis",
                    "targets": 2,
                    "width": "20%"
                },
                {
                    "data": "name",
                    "targets": 3,
                    "width": "50%"
                },
                {
                    "data": "tempat_lahir",
                    "targets": 4,
                    "width": "20%"
                },
                {
                    "data": "tanggal_lahir",
                    "targets": 5,
                    "width": "20%"
                },
                {
                    "data": "agama",
                    "targets": 6,
                    "width": "20%"
                },
                {
                    "data": "jenis_kelamin",
                    "targets": 7,
                    "width": "20%"
                },
                {
                    "data": "fingerprint",
                    "targets": 8,
                    "width": "20%"
                },
                {
                    "data": "keterangan",
                    "targets": 9,
                    "width": "30%"
                },
            ],
            "scrollX": true,
            "order": [],
            "fnDrawCallback": function() {
                if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $(".dataTables_paginate").css("display", "block");
                } else {
                    $(".dataTables_paginate").css("display", "none");
                }

                if ($("#session_level").val() == "1" || $("#session_level").val() == "2") {
                   
                } else {
                    $(".newdata").hide();
                }
            }
        });

        $('#btSubmit').click(function(e) {
            event.preventDefault();
            $("#formData").submit();
        });

        $("#formData").submit(function(e) {
            e.preventDefault();
            if (CheckRequired()) {
                if (CheckValidate()) {
                    var idData = ($("#id").val() != "" && $("#id").val() != undefined) ? $("#id").val() : null;
                    $.ajax({
                        type: 'POST',
                        url: window.location.href + "/Save",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: idData,
                            data: $("#formData").serialize()
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                $('#crudModal').modal('hide');
                                swal("Berhasil disimpan !", response.messages, "success", ).then(function() {
                                    location.reload();
                                });
                            } else {
                                swal("Gagal !", response.messages, "error", ).then(function() {
                                    location.reload();
                                });
                            }
                        }
                    });
                }
            }
        });

        $("#tbdata tbody").on("click", "#btedit", function(event) {
            event.preventDefault();
            $("#crudLabel").text("Edit Data");
            $("#action").val("edit");
            InitElement("edit");
            var data = tbdata.row($(this).parents('tr')).data();
            $.ajax({
                url: window.location.href + "/Edit",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: data.ID
                },
                dataType: 'json',
                success: function(response) {
                    $("#id").val(response.ID);
                    $("#nis").val(response.NIS);
                    $("#name").val(response.Name);
                    $("#birth_place").val(response.Birth_Place);
                    $("#birth_date").val(response.Birth_Date);
                    $("#agama").val(response.Agama).trigger("change");
                    $("#gender").val(response.Gender).trigger("change");
                    $("#fingerprint_code").val(response.Fingerprint);
                    $("#keterangan").val(response.Keterangan);
                    $("#crudModal").modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal(
                        "Gagal !",
                        textStatus + " : " + errorThrown,
                        "error"
                    ).then(function() {
                        location.reload();
                    });
                }
            });

        });


        $("#tbdata tbody").on("click", "#btdelete", function(event) {
            event.preventDefault();
            var data = tbdata.row($(this).parents('tr')).data();
            swal({
                title: "Hapus data ?",
                text: "Anda tidak akan dapat mengembalikan ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
            }).then(function(inputValue) {
                if (inputValue.value) {
                    $.ajax({
                        url: window.location.href + "/Delete",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: data.ID
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status) {
                                swal(
                                    "Terhapus",
                                    "Data berhasil dihapus.",
                                    "success"
                                ).then(function() {
                                    location.reload();
                                });
                            } else {
                                swal(
                                    "Gagal !",
                                    response.messages,
                                    "error"
                                ).then(function() {
                                    location.reload();
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal(
                                "Gagal !",
                                textStatus + " : " + errorThrown,
                                "error"
                            ).then(function() {
                                location.reload();
                            });
                        }
                    });
                }
            })
        });
    });

    function CheckRequired() {
        var fields = document.getElementById("formData").querySelectorAll("[required]");
        var result = true;
        $.each(fields, function(i, field) {
            if (!field.value) {
                toastr.error(field.name.charAt(0).toUpperCase() + field.name.slice(1) + ' tidak boleh kosong');
                result = false;
            }
        });
        return result;
    }

    function InitElement(action) {
        if (action == "add") {
            $("#id").val("");
            $("#formData").each(function() {
                $(this).find('.form-control').val("");
            });
            $("#agama").val("").trigger("change");
            $("#gender").val("").trigger("change");
            $("#nis").prop('disabled', false);
            $('#birth_date').data('daterangepicker').setStartDate(new Date());
        } else if (action == "edit") {
            $("#nis").prop('disabled', true);
        }
    }

    function CheckValidate() {

        var message = "";
        var result = true;

        if (!result) {
            toastr.error(message);
            return false;
        } else {
            return true;
        }
    }

    function GetListAgama() {
        $('#agama').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('/selectlist/GetListAgama') }}",
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#agama").append(newOption).trigger('change');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar agama');
            }
        });

    }

    function GetListGender() {
        $('#gender').empty().trigger("change");
        $.ajax({
            type: 'GET',
            url: "{{ url('/selectlist/GetListGender') }}",
            dataType: 'json',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var newOption = new Option(data[i].text, data[i].value, true, true);
                    $("#gender").append(newOption).trigger('change');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Gagal mendapatkan data daftar jenis kelamin');
            }
        });

    }

</script>
</body>

</html>
