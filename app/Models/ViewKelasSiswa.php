<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewKelasSiswa extends Model {

    public $timestamps = false;
    protected $table = 'view_list_kelas_siswa';

    public static function getByKelasId($ID){
        $filter = ['kelas_id' => $ID];
        $results = ViewKelasSiswa::where($filter)->get();

        return $results;
    }
    public static function getByKelasCode($code){
        $filter = ['kelas_code' => $code];
        $results = ViewKelasSiswa::where($filter)->get();

        return $results;
    }

    public static function getBySiswaNIS($nis){
        $filter = ['siswa_nis' => $nis];
        $results = ViewKelasSiswa::where($filter)->orderBy('kelas_tahun_ajaran', 'DESC')->orderBy('kelas_semester', 'DESC')->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewKelasSiswa::where($filter)->first();

        return $results;
    }
}
