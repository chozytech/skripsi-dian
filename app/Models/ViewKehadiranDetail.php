<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewKehadiranDetail extends Model {

    public $timestamps = false;
    protected $table = 'view_list_kehadiran_detail';

    public static function getByKehadiranId($ID){
        $filter = ['kehadiran_id' => $ID];
        $results = ViewKehadiranDetail::where($filter)->get();

        return $results;
    }

    public static function getBySiswaNIS($nis){
        $filter = ['siswa_nis' => $nis];
        $results = ViewKehadiranDetail::where($filter)->orderBy('ID', 'DESC')->get();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewKehadiranDetail::where($filter)->first();

        return $results;
    }
}
