<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LevelModels extends Model
{
    public $timestamps = false;
    protected $table = "view_list_level";

    public static function getById($id){
        $filter = ['ID' => $id];
        $results = LevelModels::where($filter)->first();

        return $results;
    }
    public static function getByCode($code){
        $filter = ['code' => $code];
        $results = LevelModels::where($filter)->first();

        return $results;
    }
}
