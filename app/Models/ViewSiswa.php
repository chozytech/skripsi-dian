<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewSiswa extends Model {

    public $timestamps = false;
    protected $table = 'view_list_siswa';

    public static function getByNIS($nis){
        $filter = ['nis' => $nis];
        $results = ViewSiswa::where($filter)->first();

        return $results;
    }

    public static function getByEnroll($enroll){
        $filter = ['fingerprint' => $enroll];
        $results = ViewSiswa::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewSiswa::where($filter)->first();

        return $results;
    }
}
