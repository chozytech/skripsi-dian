<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewWaliMurid extends Model {

    public $timestamps = false;
    protected $table = 'view_list_wali_murid';

    public static function getByCode($code){
        $filter = ['code' => $code];
        $results = ViewWaliMurid::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewWaliMurid::where($filter)->first();

        return $results;
    }
}
