<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewKelas extends Model {

    public $timestamps = false;
    protected $table = 'view_list_kelas';

    public static function getByCode($code){
        $filter = ['code' => $code];
        $results = ViewKelas::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewKelas::where($filter)->first();

        return $results;
    }
}
