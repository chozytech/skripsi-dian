<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalDetailModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_jadwal_detail";
}
