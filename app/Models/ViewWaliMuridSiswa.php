<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewWaliMuridSiswa extends Model {

    public $timestamps = false;
    protected $table = 'view_list_wali_murid_siswa';

    public static function getByWaliMuridId($ID){
        $filter = ['wali_murid_id' => $ID];
        $results = ViewWaliMuridSiswa::where($filter)->get();

        return $results;
    }
    public static function getByWaliMuridCode($code){
        $filter = ['wali_murid_code' => $code];
        $results = ViewWaliMuridSiswa::where($filter)->get();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewWaliMuridSiswa::where($filter)->first();

        return $results;
    }
}
