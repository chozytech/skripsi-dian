<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaliMuridModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_wali_murid";
}
