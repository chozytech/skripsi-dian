<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengumumanModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_pengumuman";
}
