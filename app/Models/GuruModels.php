<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuruModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_guru";
}
