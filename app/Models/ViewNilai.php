<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewNilai extends Model {

    public $timestamps = false;
    protected $table = 'view_list_nilai';


    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewNilai::where($filter)->first();

        return $results;
    }

    public static function getBySiswaNIS($nis){
        $filter = ['siswa_nis' => $nis];
        $results = ViewNilai::where($filter)->get();

        return $results;
    }
}
