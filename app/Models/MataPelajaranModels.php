<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MataPelajaranModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_mata_pelajaran";
}
