<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewJadwalDetail extends Model {

    public $timestamps = false;
    protected $table = 'view_list_jadwal_detail';

    public static function getByJadwalId($ID){
        $filter = ['jadwal_id' => $ID];
        $results = ViewJadwalDetail::where($filter)->get();

        return $results;
    }
    public static function getByJadwalIdAgama($ID,$agama){
        $filter = ['jadwal_id' => $ID];
        $results = ViewJadwalDetail::where($filter)->where('mata_pelajaran_name', 'not like', "%agama%")->orwhere('mata_pelajaran_name', 'not like', "%agama%")->orwhere('mata_pelajaran_code', 'like', "%" . $agama ."%")->orwhere('mata_pelajaran_name', 'like', "%" . $agama ."%")->get();

        return $results;
    }
    public static function getByJadwalCode($code){
        $filter = ['jadwal_code' => $code];
        $results = ViewJadwalDetail::where($filter)->get();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewJadwalDetail::where($filter)->first();

        return $results;
    }
}
