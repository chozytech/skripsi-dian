<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewMataPelajaran extends Model {

    public $timestamps = false;
    protected $table = 'view_list_mata_pelajaran';

    public static function getByCode($code){
        $filter = ['code' => $code];
        $results = ViewMataPelajaran::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewMataPelajaran::where($filter)->first();

        return $results;
    }
}
