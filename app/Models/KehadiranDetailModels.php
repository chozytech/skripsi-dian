<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KehadiranDetailModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_kehadiran_detail";
}
