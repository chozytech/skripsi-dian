<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KehadiranModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_kehadiran";
}
