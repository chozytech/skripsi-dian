<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewPengumuman extends Model {

    public $timestamps = false;
    protected $table = 'view_list_pengumuman';

    public static function getByCode($code){
        $filter = ['code' => $code];
        $results = ViewPengumuman::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewPengumuman::where($filter)->first();

        return $results;
    }
    public static function getByKelas($class){
        $filter = ['is_all' => 1];
        $results = ViewPengumuman::where($filter)->orwhere('kelas_id', $class)->get();

        return $results;
    }
}
