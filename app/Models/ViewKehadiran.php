<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewKehadiran extends Model {

    public $timestamps = false;
    protected $table = 'view_list_kehadiran';

    public static function getByCode($code){
        $filter = ['code' => $code];
        $results = ViewKehadiran::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewKehadiran::where($filter)->first();

        return $results;
    }
}
