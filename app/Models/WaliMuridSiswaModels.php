<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaliMuridSiswaModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_wali_murid_siswa";
}
