<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewJadwal extends Model {

    public $timestamps = false;
    protected $table = 'view_list_jadwal';

    public static function getByCode($code){
        $filter = ['code' => $code];
        $results = ViewJadwal::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewJadwal::where($filter)->first();

        return $results;
    }
    public static function getByKelas($class){
        $filter = ['kelas_id' => $class];
        $results = ViewJadwal::where($filter)->first();

        return $results;
    }
}
