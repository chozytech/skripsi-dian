<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewNilaiDetail extends Model {

    public $timestamps = false;
    protected $table = 'view_list_nilai_detail';

    public static function getByNilaiId($ID){
        $filter = ['nilai_id' => $ID];
        $results = ViewNilaiDetail::where($filter)->get();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewNilaiDetail::where($filter)->first();

        return $results;
    }
}
