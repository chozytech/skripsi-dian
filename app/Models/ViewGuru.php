<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewGuru extends Model {

    public $timestamps = false;
    protected $table = 'view_list_guru';

    public static function getByNIP($nip){
        $filter = ['nip' => $nip];
        $results = ViewGuru::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewGuru::where($filter)->first();

        return $results;
    }
}
