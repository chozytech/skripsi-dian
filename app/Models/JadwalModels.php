<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_jadwal";
}
