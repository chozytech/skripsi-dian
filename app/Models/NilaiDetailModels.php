<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NilaiDetailModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_nilai_detail";
}
