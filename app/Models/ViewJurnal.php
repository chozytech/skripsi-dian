<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ViewJurnal extends Model {

    public $timestamps = false;
    protected $table = 'view_list_jurnal';

    public static function getByID($id){
        $filter = ['ID' => $id];
        $results = ViewJurnal::where($filter)->first();

        return $results;
    }
    public static function getBySiswaNIS($nis){
        $filter = ['siswa_nis' => $nis];
        $results = ViewJurnal::where($filter)->get();

        return $results;
    }
}
