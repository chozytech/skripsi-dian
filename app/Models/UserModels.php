<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModels extends Model
{
    public $timestamps = false;
    protected $table = "tb_user";

    public static function login($username,$password){
        $filter = ['username' => $username, 'password' => md5($password)];
        $results = UserModels::where($filter)->first();

        return $results;
    }
}

class ViewUser extends Model {

    public $timestamps = false;
    protected $table = 'view_list_user';

    public static function login($username,$password){
        $filter = ['username' => $username, 'password' => md5($password)];
        $results = ViewUser::where($filter)->first();

        return $results;
    }

    public static function getByUsername($username){
        $filter = ['username' => $username];
        $results = ViewUser::where($filter)->first();

        return $results;
    }

    public static function getByID($id){
        $filter = ['user_id' => $id];
        $results = ViewUser::where($filter)->first();

        return $results;
    }
}
