<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\MessageEnum;
use App\Enums\PageEnum;
use App\Models\UserModels;
use App\Models\ViewUser;
use App\Models\LevelModels;
use App\Models\ViewWaliMuridSiswa;
use Session;

class AuthController extends Controller
{

    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (Session::get('is_login')) {
            return redirect('/home');
        }
    }
    public function index()
    {
        Session::put('company_name', ConfigEnum::company_name()->getValue());
        Session::put('page', PageEnum::login_page()->getValue());
        if (Session::get('is_login')) {
            $dataLevel = Session::get('level_id');
            if ($dataLevel == 1){
                return redirect('/masteruser');
            }
            else  if ($dataLevel == 2){
                return redirect('/mastersiswa');
            }
            else if ($dataLevel== 3){
                return redirect('/transpengumuman');

            }
            else{
                return redirect('/masteruser');
            }
        } else {
            return view('auth.login');
        }
    }

    public function login(Request $request)
    {
        $postData = $request->all();
        $data =  ViewUser::login($postData['username'], $postData['password']);

        if (isset($data)) {
            $this->CreateSessionLogin($request, $data);
            $this->CreateAlert($request, MessageEnum::login_success()->getValue(), "Info", $data["username"]);
            $dataLevel = LevelModels::getbyId($data['level_id']);
            if ($dataLevel['ID'] == 1){
                return redirect('/masteruser');
            }
            else  if ($dataLevel['ID'] == 2){
                return redirect('/mastersiswa');
            }
            else if ($dataLevel['ID'] == 3){
                return redirect('/transpengumuman');

            }
            else{
                return redirect('/masteruser');
            }
            
        } else {

            $this->CreateAlert($request, MessageEnum::login_failed()->getValue(), "Error");

            return redirect('/');
        }
    }

    private function CreateAlert($request, $messages, $type = "Success", $added = null)
    {
        if ($request->session()->has('alert'))
            $request->session()->forget('alert');

        if ($request->session()->has('alert_types'))
            $request->session()->forget('alert_types');

        if ($request->session()->has('alert_messages'))
            $request->session()->forget('alert_messages');

        $request->session()->put('alert', true);
        $request->session()->put('alert_types', $type);
        $request->session()->put('alert_messages', $messages . $added);
    }

    private function CreateSessionLogin($request, $data)
    {

        $dataLevel = LevelModels::getbyId($data['level_id']);

        if ($request->session()->has('is_login'))
            $request->session()->forget('is_login');

        if ($request->session()->has('username'))
            $request->session()->forget('username');

        if ($request->session()->has('name'))
            $request->session()->forget('name');

        if ($request->session()->has('level_id'))
            $request->session()->forget('level_id');
            $request->session()->put('level_id', $dataLevel['ID']);

        if ($request->session()->has('level_code'))
            $request->session()->forget('level_code');
            $request->session()->put('level_code',  $dataLevel['code']);

        if ($request->session()->has('level_name'))
            $request->session()->forget('level_name');
            $request->session()->put('level_name',  $dataLevel['name']);

        if ($dataLevel['ID'] == 3){
            $listDataSiswa = ViewWaliMuridSiswa::getByWaliMuridCode($data['username']);
            if (isset($listDataSiswa) && count($listDataSiswa) > 0){
                $dataSiswa = $listDataSiswa[0];
                if ($request->session()->has('siswa_nis'))
                $request->session()->forget('siswa_nis');
                $request->session()->put('siswa_nis',  $dataSiswa['siswa_nis']);
    
                if ($request->session()->has('siswa_name'))
                $request->session()->forget('siswa_name');
                $request->session()->put('siswa_name',  $dataSiswa['siswa_name']);
            }
        }


        $request->session()->put('is_login', true);

        $request->session()->put('username', $data['username']);
        $request->session()->put('name', $data['first_name']);





    }
}
