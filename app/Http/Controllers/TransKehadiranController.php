<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\KehadiranModels;
use App\Models\ViewKelas;
use App\Models\ViewKehadiran;
use App\Models\ViewKehadiranDetail;
use App\Models\KehadiranDetailModels;
use App\Models\ViewSiswa;
use Session;

class TransKehadiranController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::trans_kehadiran_page()->getValue());
        if (Session::get('is_login')) {
            if (Session::get('level_id') == 3) {
                return view('trans.transkehadiran.transkehadiransiswa');
            } else {
                return view('trans.transkehadiran.transkehadiran');
            }
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

          
            if (Session::get('level_id') == 3) {
                $listData = ViewKehadiranDetail::getBySiswaNIS(Session::get('siswa_nis'));
            } else {
                $listData = ViewKehadiran::all();
            }
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function GetListDetailByKehadiranId(Request $request)
    {

        $postData = $request->all();

        $listData = ViewKehadiranDetail::getByKehadiranId($postData["ID"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }


    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = KehadiranModels::where('kehadiran_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function DeleteDetail(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = KehadiranDetailModels::where('kehadiran_detail_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $dataPOST = $request->all();
        $ID = $dataPOST['id'];
        $arrPostData = [];
        parse_str($dataPOST["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        $inserted_id = null;
        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
                $inserted_id = $data['kehadiran_id'];
            } else {
                $inserted_id = $this->InsertData($data);
            }
            $result['status'] = true;
            if ($result['status']) {
                if (empty($ID)) {
                    //$data["kehadiran_id"] = ViewKehadiran::getByCode($data["kehadiran_code"])["ID"];
                    $data["kehadiran_id"] = $inserted_id;
                }
                $dataDetail = $this->MapToObjDetail(json_decode($dataPOST["detail"]), $data["kehadiran_id"]);
                for ($i = 0; $i < count($dataDetail); $i++) {
                    if ($result['status']) {
                        if (!is_null($dataDetail[$i]["kehadiran_detail_id"])) {
                            $result = $this->UpdateDataDetail($dataDetail[$i]);
                        } else {
                            $result =  $this->InsertDataDetail($dataDetail[$i]);
                        }
                    }
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $kehadiran = new KehadiranModels();

        $kehadiran->kehadiran_tanggal = empty($data['kehadiran_tanggal']) ? "" : $data['kehadiran_tanggal'];
        $kehadiran->kelas_id = empty($data['kelas_id']) ? "" : $data['kelas_id'];
        $kehadiran->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $kehadiran->created_date = date("Y-m-d H:i:s");
        $kehadiran->created_by = Session::get('username');
        $stat = $kehadiran->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            //return $result;
            return $kehadiran->id;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            // return $result;
            return null;
        }

        return null;
    }

    private function InsertDataDetail($data)
    {
        $kehadiran_detail = new KehadiranDetailModels();

        $kehadiran_detail->kehadiran_id = empty($data['kehadiran_id']) ? "" : $data['kehadiran_id'];
        $kehadiran_detail->siswa_id = empty($data['siswa_id']) ? "" : $data['siswa_id'];
        $kehadiran_detail->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $kehadiran_detail->created_date = date("Y-m-d H:i:s");
        $kehadiran_detail->created_by = Session::get('username');
        $stat = $kehadiran_detail->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'kehadiran_tanggal' => $data['kehadiran_tanggal'],
            'kelas_id' => $data['kelas_id'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = KehadiranModels::where('kehadiran_id', $data['kehadiran_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function UpdateDataDetail($data)
    {
        $updateDetails = [
            'kehadiran_id' => $data['kehadiran_id'],
            'siswa_id' => $data['siswa_id'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = KehadiranDetailModels::where('kehadiran_detail_id', $data['kehadiran_detail_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }
    private function GetDataByID($ID)
    {
        $datakehadiran = ViewKehadiran::getByID($ID);
        $Data = array(
            'ID' => $datakehadiran["ID"],
            'Tanggal' => $datakehadiran["tanggal"],
            'Kelas_ID' => $datakehadiran["kelas_id"],
            'Kelas_Code' => $datakehadiran["kelas_code"],
            'Kelas_Name' => $datakehadiran["kelas_name"],
            'Kelas_Tingkat' => $datakehadiran["kelas_tingkat"],
            'Kelas_Tahun_Ajaran' => $datakehadiran["kelas_tahun_ajaran"],
            'Kelas_Semester' => $datakehadiran["kelas_semester"],
            'Keterangan' => $datakehadiran["keterangan"],
        );
        return $Data;
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewKehadiran::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'kehadiran_id' => $exsData["ID"],
                'kehadiran_tanggal' => (!isset($Data["Tanggal"])) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $Data["Tanggal"]))),
                'kelas_id' => (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'kehadiran_id' => null,
                'kehadiran_tanggal' => (!isset($Data["Tanggal"])) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $Data["Tanggal"]))),
                'kelas_id' => (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToObjDetail($Data, $ID)
    {
        $listData = [];
        foreach ($Data as $value) {

            $exsData = ViewKehadiranDetail::getByID($value->ID);
            $newData = array();

            if (!is_null($exsData)) {
                $newData = array(
                    'kehadiran_detail_id' => $exsData["ID"],
                    'kehadiran_id' => $ID,
                    'siswa_id' => ViewSiswa::getByNIS($value->siswa_nis)["ID"],
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            } else {
                $newData = array(
                    'kehadiran_detail_id' => null,
                    'kehadiran_id' => $ID,
                    'siswa_id' => ViewSiswa::getByNIS($value->siswa_nis)["ID"],
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            }

            $listData[] = $newData;
        }
        return $listData;
    }


    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
