<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\GuruModels;
use App\Models\UserModels;
use App\Models\ViewGuru;
use Session;

class MasterGuruController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::master_guru_page()->getValue());
        if (Session::get('is_login')) {
            return view('master.masterguru.masterguru');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

            $listData = ViewGuru::all();
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $dataGuru = ViewGuru::getByID($ID);
             $stat = GuruModels::where('guru_id', $ID)
                 ->update(['is_delete' => 1]);
            if ($stat > 0) {
                 $stat = UserModels::where('username', $dataGuru["nip"])
                     ->update(['is_delete' => 1]);
                if ($stat > 0) {
                    $result['status'] = true;
                    $result['messages'] =  MessageEnum::success_delete()->getValue();
                    header('Content-Type: application/json');
                    echo json_encode($result);
                } else {
                    $result['status'] = false;
                    $result['messages'] =  MessageEnum::failed_delete()->getValue();
                    header('Content-Type: application/json');
                    echo json_encode($result);
                }
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $data = $request->all();
        $ID = $data['id'];
        $arrPostData = [];
        parse_str($data["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
                if ($result['status']){
                    $result = $this->UpdateDataUser($data);
                }
                else{
                    $result['status'] = false;
                    $result['messages'] =   MessageEnum::failed_save()->getValue();
                }
            } else {
                if ($this->CheckDoubleNIP($data["guru_nip"])) {
                    $result = $this->InsertData($data);
                    if ($result['status']){
                        $result = $this->InsertDataUser($data);
                    }
                    else{
                        $result['status'] = false;
                        $result['messages'] =   MessageEnum::failed_save()->getValue();
                    }
                } else {
                    $result['status'] = false;
                    $result['messages'] =  "NIP " . $data["guru_nip"]   . " " . MessageEnum::already_exists()->getValue();
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $guru = new GuruModels();

        $guru->guru_nip = $data['guru_nip'];
        $guru->guru_name = empty($data['guru_name']) ? "" : $data['guru_name'];
        $guru->guru_phone = empty($data['guru_phone']) ? "" : $data['guru_phone'];
        $guru->guru_email = empty($data['guru_email']) ? "" : $data['guru_email'];
        $guru->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $guru->created_date = date("Y-m-d H:i:s");
        $guru->created_by = Session::get('username');
        $stat = $guru->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function InsertDataUser($data)
    {
        $user = new UserModels();

        $user->username = $data['guru_nip'];
        $user->password = md5($data['guru_nip']);
        $user->first_name =empty($data['guru_name']) ? "" : $data['guru_name'];
        $user->level_id = 2;
        $user->phone = empty($data['guru_phone']) ? "" : $data['guru_phone'];
        $user->email = empty($data['guru_email']) ? "" : $data['guru_email'];

        $user->created_date = date("Y-m-d H:i:s");
        $user->created_by = Session::get('username');


        $stat = $user->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'guru_name' => $data['guru_name'],
            'guru_phone' => $data['guru_phone'],
            'guru_email' => $data['guru_email'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = GuruModels::where('guru_id', $data['guru_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function UpdateDataUser($data)
    {
        $updateDetails = [
            'first_name' => $data['guru_name'],
            'phone' => $data['guru_phone'],
            'email' => $data['guru_email'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = UserModels::where('username', $data['guru_nip'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function GetDataByID($ID)
    {
        $dataguru = ViewGuru::getByID($ID);
        $Data = array(
            'ID' => $dataguru["ID"],
            'NIP' => $dataguru["nip"],
            'Name' => $dataguru["name"],
            'Email' => $dataguru["email"],
            'Phone' => $dataguru["phone"],
            'Keterangan' => $dataguru["keterangan"],
        );
        return $Data;
    }

    private function CheckDoubleNIP($nip)
    {
        $dataguru = ViewGuru::getByNIP($nip);
        if (!is_null($dataguru)) {
            return false;
        } else {
            return true;
        }
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewGuru::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'guru_id' => $exsData["ID"],
                'guru_nip' => $exsData["nip"],
                'guru_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'guru_phone' => (!isset($Data["No_HP"])) ? null : $Data["No_HP"],
                'guru_email' => (!isset($Data["Email"])) ? null : $Data["Email"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'guru_id' => null,
                'guru_nip' => (!isset($Data["NIP"])) ? null : str_replace(" ", "_", strtoupper($Data["NIP"])),
                'guru_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'guru_phone' => (!isset($Data["No_HP"])) ? null : $Data["No_HP"],
                'guru_email' => (!isset($Data["Email"])) ? null : $Data["Email"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->nip;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
