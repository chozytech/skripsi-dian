<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\MataPelajaranModels;
use App\Models\ViewMataPelajaran;
use Session;

class MasterMataPelajaranController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::master_mata_pelajaran_page()->getValue());
        if (Session::get('is_login')) {
            return view('master.mastermatapelajaran.mastermatapelajaran');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

            $listData = ViewMataPelajaran::all();
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = MataPelajaranModels::where('mata_pelajaran_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $data = $request->all();
        $ID = $data['id'];
        $arrPostData = [];
        parse_str($data["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
            } else {
                if ($this->CheckDoubleCode($data["mata_pelajaran_code"])) {
                    $result = $this->InsertData($data);
                } else {
                    $result['status'] = false;
                    $result['messages'] =  "Kode " . $data["mata_pelajaran_code"]   . " " . MessageEnum::already_exists()->getValue();
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $matapelajaran = new MataPelajaranModels();

        $matapelajaran->mata_pelajaran_code = $data['mata_pelajaran_code'];
        $matapelajaran->mata_pelajaran_name = empty($data['mata_pelajaran_name']) ? "" : $data['mata_pelajaran_name'];
        $matapelajaran->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $matapelajaran->created_date = date("Y-m-d H:i:s");
        $matapelajaran->created_by = Session::get('username');
        $stat = $matapelajaran->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'mata_pelajaran_name' => $data['mata_pelajaran_name'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = MataPelajaranModels::where('mata_pelajaran_id', $data['mata_pelajaran_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function GetDataByID($ID)
    {
        $datamatapelajaran = ViewMataPelajaran::getByID($ID);
        $Data = array(
            'ID' => $datamatapelajaran["ID"],
            'Code' => $datamatapelajaran["code"],
            'Name' => $datamatapelajaran["name"],
            'Keterangan' => $datamatapelajaran["keterangan"],
        );
        return $Data;
    }

    private function CheckDoubleCode($code)
    {
        $datamatapelajaran = ViewMataPelajaran::getByCode($code);
        if (!is_null($datamatapelajaran)) {
            return false;
        } else {
            return true;
        }
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewMataPelajaran::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'mata_pelajaran_id' => $exsData["ID"],
                'mata_pelajaran_code' => $exsData["code"],
                'mata_pelajaran_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'mata_pelajaran_id' => null,
                'mata_pelajaran_code' => (!isset($Data["Kode"])) ? null : str_replace(" ", "_", strtoupper($Data["Kode"])),
                'mata_pelajaran_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
