<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\SiswaModels;
use App\Models\UserModels;
use App\Models\ViewSiswa;
use Session;

class MasterSiswaController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::master_siswa_page()->getValue());
        if (Session::get('is_login')) {
            return view('master.mastersiswa.mastersiswa');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

            $listData = ViewSiswa::all();
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $dataSiswa = ViewSiswa::getByID($ID);
            $stat = SiswaModels::where('siswa_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $data = $request->all();
        $ID = $data['id'];
        $arrPostData = [];
        parse_str($data["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
            } else {
                if ($this->CheckDoubleNIS($data["siswa_nis"])) {
                    if ($this->CheckDoubleEnroll($data["siswa_fingerprint"])) {
                        $result = $this->InsertData($data);
                    } else {
                        $result['status'] = false;
                        $result['messages'] =  "Kode Fingerprint " . $data["siswa_fingerprint"]   . " " . MessageEnum::already_exists()->getValue();
                    }
                } else {
                    $result['status'] = false;
                    $result['messages'] =  "NIS " . $data["siswa_nis"]   . " " . MessageEnum::already_exists()->getValue();
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $siswa = new SiswaModels();

        $siswa->siswa_nis = $data['siswa_nis'];
        $siswa->siswa_name = empty($data['siswa_name']) ? "" : $data['siswa_name'];
        $siswa->siswa_tanggal_lahir = empty($data['siswa_tanggal_lahir']) ? "" : $data['siswa_tanggal_lahir'];
        $siswa->siswa_tempat_lahir = empty($data['siswa_tempat_lahir']) ? "" : $data['siswa_tempat_lahir'];
        $siswa->siswa_agama = empty($data['siswa_agama']) ? "" : $data['siswa_agama'];
        $siswa->siswa_jenis_kelamin = empty($data['siswa_jenis_kelamin']) ? "" : $data['siswa_jenis_kelamin'];
        $siswa->siswa_fingerprint = empty($data['siswa_fingerprint']) ? 0 : $data['siswa_fingerprint'];
        $siswa->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $siswa->created_date = date("Y-m-d H:i:s");
        $siswa->created_by = Session::get('username');

        $stat = $siswa->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'siswa_name' => $data['siswa_name'],
            'siswa_tanggal_lahir' => $data['siswa_tanggal_lahir'],
            'siswa_tempat_lahir' => $data['siswa_tempat_lahir'],
            'siswa_agama' => $data['siswa_agama'],
            'siswa_jenis_kelamin' => $data['siswa_jenis_kelamin'],
            'siswa_fingerprint' => $data['siswa_fingerprint'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = SiswaModels::where('siswa_id', $data['siswa_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function GetDataByID($ID)
    {
        $datasiswa = ViewSiswa::getByID($ID);
        $Data = array(
            'ID' => $datasiswa["ID"],
            'NIS' => $datasiswa["nis"],
            'Name' => $datasiswa["name"],
            'Birth_Date' => $datasiswa["tanggal_lahir"],
            'Birth_Place' => $datasiswa["tempat_lahir"],
            'Agama' => $datasiswa["agama"],
            'Gender' => $datasiswa["jenis_kelamin"],
            'Fingerprint' => $datasiswa["fingerprint"],
            'Keterangan' => $datasiswa["keterangan"],
        );
        return $Data;
    }

    private function CheckDoubleNIS($nis)
    {
        $datasiswa = ViewSiswa::getByNIS($nis);
        if (!is_null($datasiswa)) {
            return false;
        } else {
            return true;
        }
    }

    private function CheckDoubleEnroll($enroll)
    {
        $datasiswa = ViewSiswa::getByEnroll($enroll);
        if (!is_null($datasiswa)) {
            return false;
        } else {
            return true;
        }
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewSiswa::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'siswa_id' => $exsData["ID"],
                'siswa_nis' => $exsData["nis"],
                'siswa_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'siswa_tanggal_lahir' => (!isset($Data["Tanggal_Lahir"])) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $Data["Tanggal_Lahir"]))),
                'siswa_tempat_lahir' => (!isset($Data["Tempat_Lahir"])) ? null : $Data["Tempat_Lahir"],
                'siswa_agama' => (!isset($Data["Agama"])) ? null : $Data["Agama"],
                'siswa_jenis_kelamin' => (!isset($Data["Jenis_Kelamin"])) ? null : $Data["Jenis_Kelamin"],
                'siswa_fingerprint' => (!isset($Data["Kode_Fingerprint"])) ? 0 : $Data["Kode_Fingerprint"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'siswa_id' => null,
                'siswa_nis' => (!isset($Data["NIS"])) ? null : str_replace(" ", "_", strtoupper($Data["NIS"])),
                'siswa_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'siswa_tanggal_lahir' => (!isset($Data["Tanggal_Lahir"])) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $Data["Tanggal_Lahir"]))),
                'siswa_tempat_lahir' => (!isset($Data["Tempat_Lahir"])) ? null : $Data["Tempat_Lahir"],
                'siswa_agama' => (!isset($Data["Agama"])) ? null : $Data["Agama"],
                'siswa_jenis_kelamin' => (!isset($Data["Jenis_Kelamin"])) ? null : $Data["Jenis_Kelamin"],
                'siswa_fingerprint' => (!isset($Data["Kode_Fingerprint"])) ? 0 : $Data["Kode_Fingerprint"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->nis;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
