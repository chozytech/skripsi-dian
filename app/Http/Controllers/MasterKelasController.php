<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\KelasModels;
use App\Models\ViewSiswa;
use App\Models\ViewKelas;
use App\Models\ViewKelasSiswa;
use App\Models\KelasSiswaModels;
use Session;

class MasterKelasController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::master_kelas_page()->getValue());
        if (Session::get('is_login')) {
            return view('master.masterkelas.masterkelas');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

            $listData = ViewKelas::all();
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function GetListSiswaByKelasId(Request $request)
    {

        $postData = $request->all();

        $listData = ViewKelasSiswa::getByKelasId($postData["ID"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function GetListSiswaByKelasCode(Request $request)
    {

        $postData = $request->all();

        $listData = ViewKelasSiswa::getByKelasCode($postData["code"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = KelasModels::where('kelas_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function DeleteSiswa(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = KelasSiswaModels::where('kelas_siswa_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $dataPOST = $request->all();
        $ID = $dataPOST['id'];
        $arrPostData = [];
        parse_str($dataPOST["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
            } else {
                if ($this->CheckDoubleCode($data["kelas_code"])) {
                    $result = $this->InsertData($data);
                } else {
                    $result['status'] = false;
                    $result['messages'] =  "Kode " . $data["kelas_code"]   . " " . MessageEnum::already_exists()->getValue();
                }
            }
            $result['status'] = true;
            if ($result['status']) {
                if (empty($ID)) {
                    $data["kelas_id"] = ViewKelas::getByCode($data["kelas_code"])["ID"];
                }
                $dataDetail = $this->MapToObjSiswa(json_decode($dataPOST["siswa"]), $data["kelas_id"]);
                for ($i = 0; $i < count($dataDetail); $i++) {
                    if ($result['status']) {
                        if (!is_null($dataDetail[$i]["kelas_siswa_id"])) {
                            $result = $this->UpdateDataSiswa($dataDetail[$i]);
                        } else {
                            $result =  $this->InsertDataSiswa($dataDetail[$i]);
                        }
                    }
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $kelas = new KelasModels();

        $kelas->kelas_code = $data['kelas_code'];
        $kelas->kelas_name = empty($data['kelas_name']) ? "" : $data['kelas_name'];
        $kelas->kelas_tingkat = empty($data['kelas_tingkat']) ? "" : $data['kelas_tingkat'];
        $kelas->kelas_tahun_ajaran = empty($data['kelas_tahun_ajaran']) ? "" : $data['kelas_tahun_ajaran'];
        $kelas->kelas_semester = empty($data['kelas_semester']) ? "" : $data['kelas_semester'];
        $kelas->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $kelas->created_date = date("Y-m-d H:i:s");
        $kelas->created_by = Session::get('username');
        $stat = $kelas->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function InsertDataSiswa($data)
    {
        $kelas_siswa = new KelasSiswaModels();

        $kelas_siswa->kelas_id = empty($data['kelas_id']) ? "" : $data['kelas_id'];
        $kelas_siswa->siswa_id = empty($data['siswa_id']) ? "" : $data['siswa_id'];
        $kelas_siswa->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $kelas_siswa->created_date = date("Y-m-d H:i:s");
        $kelas_siswa->created_by = Session::get('username');
        $stat = $kelas_siswa->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'kelas_name' => $data['kelas_name'],
            'kelas_tingkat' => $data['kelas_tingkat'],
            'kelas_tahun_ajaran' => $data['kelas_tahun_ajaran'],
            'kelas_semester' => $data['kelas_semester'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = KelasModels::where('kelas_id', $data['kelas_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function UpdateDataSiswa($data)
    {
        $updateDetails = [
            'kelas_id' => $data['kelas_id'],
            'siswa_id' => $data['siswa_id'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = KelasSiswaModels::where('kelas_siswa_id', $data['kelas_siswa_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }
    private function GetDataByID($ID)
    {
        $datakelas = ViewKelas::getByID($ID);
        $Data = array(
            'ID' => $datakelas["ID"],
            'Code' => $datakelas["code"],
            'Name' => $datakelas["name"],
            'Tingkat' => $datakelas["tingkat"],
            'Tahun_Ajaran' => $datakelas["tahun_ajaran"],
            'Semester' => $datakelas["semester"],
            'Keterangan' => $datakelas["keterangan"],
        );
        return $Data;
    }

    private function CheckDoubleCode($code)
    {
        $datakelas = ViewKelas::getByCode($code);
        if (!is_null($datakelas)) {
            return false;
        } else {
            return true;
        }
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewKelas::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'kelas_id' => $exsData["ID"],
                'kelas_code' => $exsData["code"],
                'kelas_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'kelas_tingkat' => (!isset($Data["Tingkat"])) ? null : $Data["Tingkat"],
                'kelas_tahun_ajaran' => (!isset($Data["Tahun_Ajaran"])) ? null : $Data["Tahun_Ajaran"],
                'kelas_semester' => (!isset($Data["Semester"])) ? null : $Data["Semester"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'kelas_id' => null,
                'kelas_code' => (!isset($Data["Kode"])) ? null : str_replace(" ", "_", strtoupper($Data["Kode"])),
                'kelas_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'kelas_tingkat' => (!isset($Data["Tingkat"])) ? null : $Data["Tingkat"],
                'kelas_tahun_ajaran' => (!isset($Data["Tahun_Ajaran"])) ? null : $Data["Tahun_Ajaran"],
                'kelas_semester' => (!isset($Data["Semester"])) ? null : $Data["Semester"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToObjSiswa($Data, $ID)
    {
        $listData = [];
        foreach ($Data as $value) {

            $exsData = ViewKelasSiswa::getByID($value->ID);
            $newData = array();

            if (!is_null($exsData)) {
                $newData = array(
                    'kelas_siswa_id' => $exsData["ID"],
                    'kelas_id' => $ID,
                    'siswa_id' => ViewSiswa::getByNIS($value->siswa_nis)["ID"],
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            } else {
                $newData = array(
                    'kelas_siswa_id' => null,
                    'kelas_id' => $ID,
                    'siswa_id' => ViewSiswa::getByNIS($value->siswa_nis)["ID"],
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            }

            $listData[] = $newData;
        }
        return $listData;
    }


    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
