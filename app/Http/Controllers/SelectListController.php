<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Http\Helpers\SelectList;
use Session;
use App\Models\LevelModels;
use App\Models\UserModels;
use App\Models\ViewUser;
use App\Models\ViewLevel;
use App\Models\ViewSiswa;
use App\Models\ViewWaliMuridSiswa;
use App\Models\ViewKelas;
use App\Models\ViewKelasSiswa;
use App\Models\ViewMataPelajaran;

class SelectListController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        return redirect('/');
    }

    public function GetListLevel()
	{
        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listLevel = LevelModels::all();
        $result = $this->MapToSelectList($listLevel,"level");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListSiswa()
	{
        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listSiswa = ViewSiswa::all();
        $result = $this->MapToSelectList($listSiswa,"siswa");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListTingkat()
	{
        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listTingkat = [];
        for ($i=0;$i<6;$i++){
            $listTingkat[] = ($i+1);
        }
        $result = $this->MapToSelectList($listTingkat,"tingkat");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListSemester()
	{
        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listSemester = [];
        for ($i=0;$i<2;$i++){
            $listSemester[] = ($i+1);
        }
        $result = $this->MapToSelectList($listSemester,"semester");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListAgama()
	{
        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listAgama = ["Islam", "Kristen", "Katolik", "Hindu", "Budha", "Konghucu"];
        $result = $this->MapToSelectList($listAgama,"agama");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListGender()
	{

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listGender = ["Laki - Laki", "Perempuan"];
        $result = $this->MapToSelectList($listGender,"gender");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListKelas()
	{
        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listKelas = ViewKelas::all();
        $result = $this->MapToSelectList($listKelas,"kelas");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListMataPelajaran()
	{
        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listMataPelajaran = ViewMataPelajaran::all();
        $result = $this->MapToSelectList($listMataPelajaran,"mata_pelajaran");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListHari()
	{

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $listHari = ["Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu"];
        $result = $this->MapToSelectList($listHari,"hari");

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListSiswaByKelasCode(Request $request)
    {

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $postData = $request->all();

        $listData = ViewKelasSiswa::getByKelasCode($postData["Code"]);

        $result = $this->MapToSelectList($listData,"siswa_kehadiran");

        header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function GetListSiswaByWaliMuridCode(Request $request)
    {

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
        $postData = $request->all();

        $listData = ViewWaliMuridSiswa::getByWaliMuridCode($postData["Code"]);

        $result = $this->MapToSelectList($listData,"siswa_kehadiran");

        header('Content-Type: application/json');
		echo json_encode($result);
    }
    

    private function MapToSelectList($Data, $Type=null)
	{
		$retData = [];
		$defaultItem = new SelectList();
		$defaultItem->value = "";
		$defaultItem->text = "-- Select Item --";
		$retData[] = $defaultItem;
		if (!is_null($Data)) {
			foreach ($Data as $value) {
				$item = new SelectList();
				if ($Type == "siswa"){
                    $item->value = $value->nis;
					$item->text = $value->name;
                }
                else if ($Type == "siswa_kehadiran"){
                    $item->value = $value->siswa_nis;
					$item->text = $value->siswa_name;
                }
                else if ($Type == "tingkat" || $Type == "semester" || $Type == "agama" || $Type == "gender" || $Type == "hari"){
                    $item->value = $value;
					$item->text = $value;
                }
                
				else{
					$item->value = $value->code;
					$item->text = $value->name;
				}
				$retData[] = $item;
			}
		}
		return $retData;
	}
}
