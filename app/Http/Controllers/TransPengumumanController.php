<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\PengumumanModels;
use App\Models\ViewPengumuman;
use App\Models\ViewKelas;
use App\Models\ViewKelasSiswa;
use Session;

class TransPengumumanController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::trans_pengumuman_page()->getValue());
        if (Session::get('is_login')) {
            return view('trans.transpengumuman.transpengumuman');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {
            if (Session::get('level_id') == 3) {
                $dataKelas = ViewKelasSiswa::getBySiswaNIS(Session::get('siswa_nis'));
                $listData = ViewPengumuman::getByKelas($dataKelas['kelas_id']);
            } else {
                $listData = ViewPengumuman::all();
            }
           
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = PengumumanModels::where('pengumuman_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $data = $request->all();
        $ID = $data['id']  == "null" || $data['id'] == "" ? null : $data['id'];
        $data = $this->MapToObj($data, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
            } else {
                $result = $this->InsertData($data);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $pengumuman = new PengumumanModels();

        $pengumuman->pengumuman_code = empty($data['pengumuman_code']) ? "" : $data['pengumuman_code'];
        $pengumuman->pengumuman_name = empty($data['pengumuman_name']) ? "" : $data['pengumuman_name'];
        $pengumuman->pengumuman_file = empty($data['pengumuman_file']) ? "" : $data['pengumuman_file'];
        $pengumuman->kelas_id = empty($data['kelas_id']) ? null : $data['kelas_id'];
        $pengumuman->is_all = empty($data['is_all']) ? null : $data['is_all'];
        $pengumuman->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $pengumuman->created_date = date("Y-m-d H:i:s");
        $pengumuman->created_by = Session::get('username');
        $stat = $pengumuman->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'pengumuman_code' => $data['pengumuman_code'],
            'pengumuman_name' => $data['pengumuman_name'],
            'pengumuman_file' => $data['pengumuman_file'],
            'kelas_id' => $data['kelas_id'],
            'is_all' => $data['is_all'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = PengumumanModels::where('pengumuman_id', $data['pengumuman_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function GetDataByID($ID)
    {
        $datapengumuman = ViewPengumuman::getByID($ID);
        $Data = array(
            'ID' => $datapengumuman["ID"],
            'Code' => $datapengumuman["code"],
            'Name' => $datapengumuman["name"],
            'File' => $datapengumuman["file"],
            'Is_All' => $datapengumuman["is_all"],
            'Kelas_ID' => $datapengumuman["kelas_id"],
            'Kelas_Code' => $datapengumuman["kelas_code"],
            'Kelas_Name' => $datapengumuman["kelas_name"],
            'Kelas_Tingkat' => $datapengumuman["kelas_tingkat"],
            'Kelas_Tahun_Ajaran' => $datapengumuman["kelas_tahun_ajaran"],
            'Kelas_Semester' => $datapengumuman["kelas_semester"],
            'Keterangan' => $datapengumuman["keterangan"],
        );
        return $Data;
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewPengumuman::getByID($ID);
        $retData = array();
        $targetFile = null;

        if (!empty($_FILES)) {
            if (!is_dir("assets/uploads/")) {
                mkdir("./" . "assets/uploads/", 0777, TRUE);
            }
            if (!empty($_FILES['File']['tmp_name'])) {
                $tempFile = $_FILES['File']['tmp_name'];
                $extension = "." . pathinfo($_FILES['File']['name'], PATHINFO_EXTENSION);
                $targetFile = "assets/uploads/" . date('YmdHis') . $extension;
                if (file_exists($targetFile)) unlink($targetFile);
                move_uploaded_file($tempFile, $targetFile);
            }
        }

        if (!is_null($exsData)) {
            $retData = array(
                'pengumuman_id' => $exsData["ID"],
                'pengumuman_code' => $exsData["code"],
                'pengumuman_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'pengumuman_file' => (!isset($targetFile)) ? $exsData["file"] : $targetFile,
                'is_all' => (!isset($Data["Is_All"])) ? null : $Data["Is_All"],
                'kelas_id' => (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'pengumuman_id' => null,
                'pengumuman_code' => (!isset($Data["Kode"])) ? null : $Data["Kode"],
                'pengumuman_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'is_all' => (!isset($Data["Is_All"])) ? null : $Data["Is_All"],
                'pengumuman_file' => (!isset($targetFile)) ? null : $targetFile,
                'kelas_id' => (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
