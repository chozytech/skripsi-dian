<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Models\ViewSiswa;
use Session;

class ChangeSiswaController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        return redirect('/');
    }
    public function SwitchSiswa(Request $request){
        $nis = $request->input('nis');
        $dataSiswa = ViewSiswa::getByNIS($nis);
        if ($request->session()->has('siswa_nis'))
        $request->session()->forget('siswa_nis');
        $request->session()->put('siswa_nis',  $dataSiswa['nis']);

        if ($request->session()->has('siswa_name'))
        $request->session()->forget('siswa_name');
        $request->session()->put('siswa_name',  $dataSiswa['name']);

        return redirect('/');
    }
}
