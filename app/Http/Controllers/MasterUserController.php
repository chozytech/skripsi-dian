<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\LevelModels;
use App\Models\UserModels;
use App\Models\ViewUser;
use Session;

class MasterUserController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::master_user_page()->getValue());
        if (Session::get('is_login')) {
            return view('master.masteruser.masteruser');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

            $listData = ViewUser::all();
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = UserModels::where('user_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $data = $request->all();
        $ID = $data['id'];
        $arrPostData = [];
        parse_str($data["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
            } else {
                if ($this->CheckDoubleUsername($data["username"])) {
                    $result = $this->InsertData($data);
                } else {
                    $result['status'] = false;
                    $result['messages'] =  "Username " . $data["username"]   . " " . MessageEnum::already_exists()->getValue();
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $user = new UserModels();

        $user->username = $data['username'];
        $user->password = md5($data['username']);
        $user->first_name =empty($data['first_name']) ? "" : $data['first_name'];
        $user->last_name = empty($data['last_name']) ? "" : $data['last_name'];
        $user->level_id = $data['level_id'];
        $user->phone = empty($data['phone']) ? "" : $data['phone'];
        $user->email = empty($data['email']) ? "" : $data['email'];
        $user->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $user->created_date = date("Y-m-d H:i:s");
        $user->created_by = Session::get('username');

        $stat = $user->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'level_id' => $data['level_id'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = UserModels::where('user_id', $data['user_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function GetDataByID($ID)
    {
        $datauser = ViewUser::getByID($ID);
        $Data = array(
            'User_ID' => $datauser["user_id"],
            'Username' => $datauser["username"],
            'First_Name' => $datauser["first_name"],
            'Last_Name' => $datauser["last_name"],
            'Level_Code' => $datauser["level_code"],
            'Email' => $datauser["email"],
            'Phone' => $datauser["phone"],
            'Keterangan' => $datauser["keterangan"],
        );
        return $Data;
    }

    private function CheckDoubleUsername($Username)
    {
        $datauser = ViewUser::getByUsername($Username);
        if (!is_null($datauser)) {
            return false;
        } else {
            return true;
        }
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewUser::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'user_id' => $exsData["user_id"],
                'username' => $exsData["username"],
                'first_name' => (!isset($Data["Nama_Depan"])) ? null : $Data["Nama_Depan"],
                'last_name' => (!isset($Data["Nama_Belakang"])) ? null : $Data["Nama_Belakang"],
                'level_id' => (!isset($Data["Akses"])) ? null : LevelModels::getByCode($Data["Akses"])["ID"],
                'phone' => (!isset($Data["No_HP"])) ? null : $Data["No_HP"],
                'email' => (!isset($Data["Email"])) ? null : $Data["Email"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'user_id' => null,
                'username' => (!isset($Data["Nama_Pengguna"])) ? null : str_replace(" ", "_", strtoupper($Data["Nama_Pengguna"])),
                'first_name' => (!isset($Data["Nama_Depan"])) ? null : $Data["Nama_Depan"],
                'last_name' => (!isset($Data["Nama_Belakang"])) ? null : $Data["Nama_Belakang"],
                'level_id' => (!isset($Data["Akses"])) ? null : LevelModels::getByCode($Data["Akses"])["ID"],
                'phone' => (!isset($Data["No_HP"])) ? null : $Data["No_HP"],
                'email' => (!isset($Data["Email"])) ? null : $Data["Email"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
