<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\JurnalModels;
use App\Models\ViewJurnal;
use App\Models\ViewSiswa;
use Session;

class TransJurnalController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::trans_jurnal_page()->getValue());
        if (Session::get('is_login')) {
            return view('trans.transjurnal.transjurnal');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {
            if (Session::get('level_id') == 3) {
                $listData = ViewJurnal::getBySiswaNIS(Session::get('siswa_nis'));
            } else {
                $listData = ViewJurnal::all();
            }
          
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = JurnalModels::where('jurnal_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $data = $request->all();
       // $ID = $data['id'];
       // $arrPostData = [];
       // parse_str($data["data"], $arrPostData);
       // $data = $this->MapToObj($arrPostData, $ID);
       $ID = $data['id']  == "null" || $data['id'] == "" ? null : $data['id'];
       $data = $this->MapToObj($data, $ID);


        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
            } else {
                $result = $this->InsertData($data);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $jurnal = new JurnalModels();

        $jurnal->jurnal_tanggal = empty($data['jurnal_tanggal']) ? "" : $data['jurnal_tanggal'];
        $jurnal->siswa_id = empty($data['siswa_id']) ? "" : $data['siswa_id'];
        $jurnal->jurnal_jam = empty($data['jurnal_jam']) ? "" : $data['jurnal_jam'];
        $jurnal->jurnal_file = empty($data['jurnal_file']) ? "" : $data['jurnal_file'];
        $jurnal->catatan = empty($data['catatan']) ? "" : $data['catatan'];
        $jurnal->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $jurnal->created_date = date("Y-m-d H:i:s");
        $jurnal->created_by = Session::get('username');
        $stat = $jurnal->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'jurnal_tanggal' => $data['jurnal_tanggal'],
            'jurnal_jam' => $data['jurnal_jam'],
            'jurnal_file' => $data['jurnal_file'],
            'siswa_id' => $data['siswa_id'],
            'catatan' => $data['catatan'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = JurnalModels::where('jurnal_id', $data['jurnal_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function GetDataByID($ID)
    {
        $datajurnal = ViewJurnal::getByID($ID);
        $Data = array(
            'ID' => $datajurnal["ID"],
            'Tanggal' => $datajurnal["tanggal"],
            'Jam' => $datajurnal["jam"],
            'File' => $datajurnal["file"],
            'Created_By' => $datajurnal["created_by"],
            'Modified_By' => $datajurnal["modified_by"],
            'Siswa_ID' => $datajurnal["siswa_id"],
            'Siswa_NIS' => $datajurnal["siswa_nis"],
            'Siswa_Name' => $datajurnal["siswa_name"],
            'Catatan' => $datajurnal["catatan"],
            'Keterangan' => $datajurnal["keterangan"],
        );
        return $Data;
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewJurnal::getByID($ID);
        $retData = array();

        $targetFile = null;

        if (!empty($_FILES)) {
            if (!is_dir("assets/uploads/")) {
                mkdir("./" . "assets/uploads/", 0777, TRUE);
            }
            if (!empty($_FILES['File']['tmp_name'])) {
                $tempFile = $_FILES['File']['tmp_name'];
                $extension = "." . pathinfo($_FILES['File']['name'], PATHINFO_EXTENSION);
                $targetFile = "assets/uploads/" . date('YmdHis') . $extension;
                if (file_exists($targetFile)) unlink($targetFile);
                move_uploaded_file($tempFile, $targetFile);
            }
        }

        if (!is_null($exsData)) {
            $retData = array(
                'jurnal_id' => $exsData["ID"],
                'jurnal_tanggal' => (!isset($Data["Tanggal"])) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $Data["Tanggal"]))), 
                'jurnal_jam' => (!isset($Data["Jam"])) ? "" : $Data["Jam"],
                'jurnal_file' => (!isset($targetFile)) ? $exsData["file"] : $targetFile,
                'siswa_id' => (!isset($Data["Siswa"])) ? null : ViewSiswa::getByNIS($Data["Siswa"])["ID"],
                'catatan' => (!isset($Data["Catatan"])) ? "" : $Data["Catatan"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'jurnal_id' => null,
                'jurnal_tanggal' => (!isset($Data["Tanggal"])) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $Data["Tanggal"]))), 
                'jurnal_jam' => (!isset($Data["Jam"])) ? "" : $Data["Jam"],
                'jurnal_file' => (!isset($targetFile)) ? "" : $targetFile,
                'siswa_id' => (!isset($Data["Siswa"])) ? null : ViewSiswa::getByNIS($Data["Siswa"])["ID"],
                'catatan' => (!isset($Data["Catatan"])) ? "" : $Data["Catatan"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
