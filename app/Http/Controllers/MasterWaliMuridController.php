<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\WaliMuridModels;
use App\Models\UserModels;
use App\Models\ViewSiswa;
use App\Models\ViewWaliMurid;
use App\Models\ViewWaliMuridSiswa;
use App\Models\WaliMuridSiswaModels;
use Session;

class MasterWaliMuridController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::master_wali_murid_page()->getValue());
        if (Session::get('is_login')) {
            return view('master.masterwalimurid.masterwalimurid');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

            $listData = ViewWaliMurid::all();
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function GetListSiswaByWaliMuridId(Request $request)
    {

        $postData = $request->all();

        $listData = ViewWaliMuridSiswa::getByWaliMuridId($postData["ID"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function GetListSiswaByWaliMuridCode(Request $request)
    {

        $postData = $request->all();

        $listData = ViewWaliMuridSiswa::getByWaliMuridCode($postData["code"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $dataWaliMurid = ViewWaliMurid::getByID($ID);
            $stat = WaliMuridModels::where('wali_murid_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $stat = UserModels::where('username', $dataWaliMurid["code"])
                     ->update(['is_delete' => 1]);
                if ($stat > 0) {
                    $result['status'] = true;
                    $result['messages'] =  MessageEnum::success_delete()->getValue();
                    header('Content-Type: application/json');
                    echo json_encode($result);
                } else {
                    $result['status'] = false;
                    $result['messages'] =  MessageEnum::failed_delete()->getValue();
                    header('Content-Type: application/json');
                    echo json_encode($result);
                }
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function DeleteSiswa(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = WaliMuridSiswaModels::where('wali_murid_siswa_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $dataPOST = $request->all();
        $ID = $dataPOST['id'];
        $arrPostData = [];
        parse_str($dataPOST["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
                if ($result['status']){
                    $result = $this->UpdateDataUser($data);
                }
                else{
                    $result['status'] = false;
                    $result['messages'] =   MessageEnum::failed_save()->getValue();
                }
            } else {
                if ($this->CheckDoubleCode($data["wali_murid_code"])) {
                    $result = $this->InsertData($data);
                    if ($result['status']){
                        $result = $this->InsertDataUser($data);
                    }
                    else{
                        $result['status'] = false;
                        $result['messages'] =   MessageEnum::failed_save()->getValue();
                    }
                } else {
                    $result['status'] = false;
                    $result['messages'] =  "KTP " . $data["wali_murid_code"]   . " " . MessageEnum::already_exists()->getValue();
                }
            }
            $result['status'] = true;
            if ($result['status']) {
                if (empty($ID)) {
                    $data["wali_murid_id"] = ViewWaliMurid::getByCode($data["wali_murid_code"])["ID"];
                }
                $dataDetail = $this->MapToObjSiswa(json_decode($dataPOST["siswa"]), $data["wali_murid_id"]);
                for ($i = 0; $i < count($dataDetail); $i++) {
                    if ($result['status']) {
                        if (!is_null($dataDetail[$i]["wali_murid_siswa_id"])) {
                            $result = $this->UpdateDataSiswa($dataDetail[$i]);
                        } else {
                            $result =  $this->InsertDataSiswa($dataDetail[$i]);
                        }
                    }
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $wali_murid = new WaliMuridModels();

        $wali_murid->wali_murid_code = $data['wali_murid_code'];
        $wali_murid->wali_murid_name = empty($data['wali_murid_name']) ? "" : $data['wali_murid_name'];
        $wali_murid->wali_murid_phone = empty($data['wali_murid_phone']) ? "" : $data['wali_murid_phone'];
        $wali_murid->wali_murid_email = empty($data['wali_murid_email']) ? "" : $data['wali_murid_email'];
        $wali_murid->wali_murid_alamat = empty($data['wali_murid_alamat']) ? "" : $data['wali_murid_alamat'];
        $wali_murid->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $wali_murid->created_date = date("Y-m-d H:i:s");
        $wali_murid->created_by = Session::get('username');
        $stat = $wali_murid->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function InsertDataSiswa($data)
    {
        $wali_murid_siswa = new WaliMuridSiswaModels();

        $wali_murid_siswa->wali_murid_id = empty($data['wali_murid_id']) ? "" : $data['wali_murid_id'];
        $wali_murid_siswa->siswa_id = empty($data['siswa_id']) ? "" : $data['siswa_id'];
        $wali_murid_siswa->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $wali_murid_siswa->created_date = date("Y-m-d H:i:s");
        $wali_murid_siswa->created_by = Session::get('username');
        $stat = $wali_murid_siswa->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function InsertDataUser($data)
    {
        $user = new UserModels();

        $user->username = $data['wali_murid_code'];
        $user->password = md5($data['wali_murid_code']);
        $user->first_name =empty($data['wali_murid_name']) ? "" : $data['wali_murid_name'];
        $user->level_id = 3;
        $user->phone = empty($data['wali_murid_phone']) ? "" : $data['wali_murid_phone'];
        $user->email = empty($data['wali_murid_email']) ? "" : $data['wali_murid_email'];

        $user->created_date = date("Y-m-d H:i:s");
        $user->created_by = Session::get('username');


        $stat = $user->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'wali_murid_name' => $data['wali_murid_name'],
            'wali_murid_phone' => $data['wali_murid_phone'],
            'wali_murid_email' => $data['wali_murid_email'],
            'wali_murid_alamat' => $data['wali_murid_alamat'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = WaliMuridModels::where('wali_murid_id', $data['wali_murid_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function UpdateDataSiswa($data)
    {
        $updateDetails = [
            'wali_murid_id' => $data['wali_murid_id'],
            'siswa_id' => $data['siswa_id'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = WaliMuridSiswaModels::where('wali_murid_siswa_id', $data['wali_murid_siswa_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function UpdateDataUser($data)
    {
        $updateDetails = [
            'first_name' => $data['wali_murid_name'],
            'phone' => $data['wali_murid_phone'],
            'email' => $data['wali_murid_email'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = UserModels::where('username', $data['wali_murid_code'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function GetDataByID($ID)
    {
        $datawali_murid = ViewWaliMurid::getByID($ID);
        $Data = array(
            'ID' => $datawali_murid["ID"],
            'Code' => $datawali_murid["code"],
            'Name' => $datawali_murid["name"],
            'Email' => $datawali_murid["email"],
            'Phone' => $datawali_murid["phone"],
            'Alamat' => $datawali_murid["alamat"],
            'Keterangan' => $datawali_murid["keterangan"],
        );
        return $Data;
    }

    private function CheckDoubleCode($code)
    {
        $datawali_murid = ViewWaliMurid::getByCode($code);
        if (!is_null($datawali_murid)) {
            return false;
        } else {
            return true;
        }
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewWaliMurid::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'wali_murid_id' => $exsData["ID"],
                'wali_murid_code' => $exsData["code"],
                'wali_murid_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'wali_murid_phone' => (!isset($Data["No_HP"])) ? null : $Data["No_HP"],
                'wali_murid_email' => (!isset($Data["Email"])) ? null : $Data["Email"],
                'wali_murid_alamat' => (!isset($Data["Alamat"])) ? null : $Data["Alamat"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'wali_murid_id' => null,
                'wali_murid_code' => (!isset($Data["KTP"])) ? null : str_replace(" ", "_", strtoupper($Data["KTP"])),
                'wali_murid_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'wali_murid_phone' => (!isset($Data["No_HP"])) ? null : $Data["No_HP"],
                'wali_murid_email' => (!isset($Data["Email"])) ? null : $Data["Email"],
                'wali_murid_alamat' => (!isset($Data["Alamat"])) ? null : $Data["Alamat"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToObjSiswa($Data, $ID)
    {
        $listData = [];
        foreach ($Data as $value) {

            $exsData = ViewWaliMuridSiswa::getByID($value->ID);
            $newData = array();

            if (!is_null($exsData)) {
                $newData = array(
                    'wali_murid_siswa_id' => $exsData["ID"],
                    'wali_murid_id' => $ID,
                    'siswa_id' => ViewSiswa::getByNIS($value->siswa_nis)["ID"],
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            } else {
                $newData = array(
                    'wali_murid_siswa_id' => null,
                    'wali_murid_id' => $ID,
                    'siswa_id' => ViewSiswa::getByNIS($value->siswa_nis)["ID"],
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            }

            $listData[] = $newData;
        }
        return $listData;
    }


    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
