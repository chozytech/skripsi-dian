<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\JadwalModels;
use App\Models\KelasModels;
use App\Models\ViewJadwal;
use App\Models\ViewJadwalDetail;
use App\Models\JadwalDetailModels;
use App\Models\ViewKelas;
use App\Models\ViewKelasSiswa;
use App\Models\ViewMataPelajaran;
use App\Models\ViewSiswa;
use Session;

class TransJadwalController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::trans_jadwal_page()->getValue());
        if (Session::get('is_login')) {
            if (Session::get('level_id') == 3) {
                return view('trans.transjadwal.transjadwalsiswa');
            } else {
                return view('trans.transjadwal.transjadwal');
            }
           
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {
            if (Session::get('level_id') == 3) {
                $dataSiswa = ViewSiswa::getByNIS(Session::get('siswa_nis'));
                $dataKelas = ViewKelasSiswa::getBySiswaNIS(Session::get('siswa_nis'));
                $dataJadwal = ViewJadwal::getByKelas($dataKelas['kelas_id']);
                if (isset($dataJadwal)){
                $listData = ViewJadwalDetail::getByJadwalIdAgama($dataJadwal['ID'],$dataSiswa['agama']);
                }
                else{
                    $listData = [];
                }
            } else {
                $listData = ViewJadwal::all();
            }
            
             $data = array(
                 'data' => $listData
             );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function GetListDetailByJadwalId(Request $request)
    {

        $postData = $request->all();

        $listData = ViewJadwalDetail::getByJadwalId($postData["ID"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function GetListDetailByJadwalCode(Request $request)
    {

        $postData = $request->all();

        $listData = ViewJadwalDetail::getByJadwalCode($postData["code"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = JadwalModels::where('jadwal_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function DeleteDetail(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = JadwalDetailModels::where('jadwal_detail_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $dataPOST = $request->all();
        $ID = $dataPOST['id'];
        $arrPostData = [];
        parse_str($dataPOST["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
            } else {
                if ($this->CheckDoubleCode($data["jadwal_code"])) {
                    $result = $this->InsertData($data);
                } else {
                    $result['status'] = false;
                    $result['messages'] =  "Kode " . $data["jadwal_code"]   . " " . MessageEnum::already_exists()->getValue();
                }
            }
            $result['status'] = true;
            if ($result['status']) {
                if (empty($ID)) {
                    $data["jadwal_id"] = ViewJadwal::getByCode($data["jadwal_code"])["ID"];
                }
                $dataDetail = $this->MapToObjDetail(json_decode($dataPOST["detail"]), $data["jadwal_id"]);
                for ($i = 0; $i < count($dataDetail); $i++) {
                    if ($result['status']) {
                        if (!is_null($dataDetail[$i]["jadwal_detail_id"])) {
                            $result = $this->UpdateDataDetail($dataDetail[$i]);
                        } else {
                            $result =  $this->InsertDataDetail($dataDetail[$i]);
                        }
                    }
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $jadwal = new JadwalModels();

        $jadwal->jadwal_code = $data['jadwal_code'];
        $jadwal->jadwal_name = empty($data['jadwal_name']) ? "" : $data['jadwal_name'];
        $jadwal->kelas_id = empty($data['kelas_id']) ? "" : $data['kelas_id'];
        $jadwal->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $jadwal->created_date = date("Y-m-d H:i:s");
        $jadwal->created_by = Session::get('username');
        $stat = $jadwal->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function InsertDataDetail($data)
    {
        $jadwal_detail = new JadwalDetailModels();

        $jadwal_detail->jadwal_id = empty($data['jadwal_id']) ? "" : $data['jadwal_id'];
        $jadwal_detail->mata_pelajaran_id = empty($data['mata_pelajaran_id']) ? "" : $data['mata_pelajaran_id'];
        $jadwal_detail->jadwal_hari = empty($data['jadwal_hari']) ? "" : $data['jadwal_hari'];
        $jadwal_detail->jadwal_jam = empty($data['jadwal_jam']) ? "" : $data['jadwal_jam'];
        $jadwal_detail->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $jadwal_detail->created_date = date("Y-m-d H:i:s");
        $jadwal_detail->created_by = Session::get('username');
        $stat = $jadwal_detail->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'jadwal_name' => $data['jadwal_name'],
            'kelas_id' => $data['kelas_id'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = JadwalModels::where('jadwal_id', $data['jadwal_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function UpdateDataDetail($data)
    {
        $updateDetails = [
            'jadwal_id' => $data['jadwal_id'],
            'mata_pelajaran_id' => $data['mata_pelajaran_id'],
            'jadwal_hari' => $data['jadwal_hari'],
            'jadwal_jam' => $data['jadwal_jam'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = JadwalDetailModels::where('jadwal_detail_id', $data['jadwal_detail_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }
    private function GetDataByID($ID)
    {
        $datajadwal = ViewJadwal::getByID($ID);
        $Data = array(
            'ID' => $datajadwal["ID"],
            'Code' => $datajadwal["code"],
            'Name' => $datajadwal["name"],
            'Kelas_ID' => $datajadwal["kelas_id"],
            'Kelas_Code' => $datajadwal["kelas_code"],
            'Kelas_Name' => $datajadwal["kelas_name"],
            'Kelas_Tingkat' => $datajadwal["kelas_tingkat"],
            'Kelas_Tahun_Ajaran' => $datajadwal["kelas_tahun_ajaran"],
            'Kelas_Semester' => $datajadwal["kelas_semester"],
            'Keterangan' => $datajadwal["keterangan"],
        );
        return $Data;
    }

    private function CheckDoubleCode($code)
    {
        $datajadwal = ViewJadwal::getByCode($code);
        if (!is_null($datajadwal)) {
            return false;
        } else {
            return true;
        }
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewJadwal::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'jadwal_id' => $exsData["ID"],
                'jadwal_code' => $exsData["code"],
                'jadwal_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'kelas_id' =>  (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'jadwal_id' => null,
                'jadwal_code' => (!isset($Data["Kode"])) ? null : str_replace(" ", "_", strtoupper($Data["Kode"])),
                'jadwal_name' => (!isset($Data["Nama"])) ? null : $Data["Nama"],
                'kelas_id' =>  (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToObjDetail($Data, $ID)
    {
        $listData = [];
        foreach ($Data as $value) {

            $exsData = ViewJadwalDetail::getByID($value->ID);
            $newData = array();

            if (!is_null($exsData)) {
                $newData = array(
                    'jadwal_detail_id' => $exsData["ID"],
                    'jadwal_id' => $ID,
                    'mata_pelajaran_id' => ViewMataPelajaran::getByCode($value->mata_pelajaran_code)["ID"],
                    'jadwal_hari' => (!isset($value->hari)) ? "" : $value->hari,
                    'jadwal_jam' => (!isset($value->jam)) ? "" : $value->jam,
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            } else {
                $newData = array(
                    'jadwal_detail_id' => null,
                    'jadwal_id' => $ID,
                    'mata_pelajaran_id' => ViewMataPelajaran::getByCode($value->mata_pelajaran_code)["ID"],
                    'jadwal_hari' => (!isset($value->hari)) ? "" : $value->hari,
                    'jadwal_jam' => (!isset($value->jam)) ? "" : $value->jam,
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            }

            $listData[] = $newData;
        }
        return $listData;
    }


    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
