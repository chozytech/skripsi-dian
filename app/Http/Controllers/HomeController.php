<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use Session;

class HomeController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::dashboard_page()->getValue());
        if (Session::get('is_login')) {
            return view('home.home');
        } else {
            return redirect('/');
        }
    }
}
