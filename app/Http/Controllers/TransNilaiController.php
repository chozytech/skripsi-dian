<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\ConfigEnum;
use App\Enums\PageEnum;
use App\Enums\MessageEnum;
use App\Http\Helpers\SelectList;
use App\Models\NilaiModels;
use App\Models\KelasModels;
use App\Models\ViewNilai;
use App\Models\ViewNilaiDetail;
use App\Models\NilaiDetailModels;
use App\Models\ViewKelas;
use App\Models\ViewMataPelajaran;
use App\Models\ViewSiswa;
use Session;

class TransNilaiController extends Controller
{
    public function __construct()
    {
        //Set Session
        Session::put('app_name', ConfigEnum::app_name()->getValue());
        Session::put('company_name', ConfigEnum::company_name()->getValue());

        if (!(Session::get('is_login'))) {
            return redirect('/');
        }
    }
    public function index()
    {
        Session::put('page', PageEnum::trans_nilai_page()->getValue());
        if (Session::get('is_login')) {
            return view('trans.transnilai.transnilai');
        } else {
            return redirect('/');
        }
    }

    public function GetListData()
    {

        if (Session::get('is_login')) {

            if (Session::get('level_id') == 3) {
                $listData = ViewNilai::getBySiswaNIS(Session::get('siswa_nis'));
            } else {
                $listData = ViewNilai::all();
            }
            $data = array(
                'data' => $listData
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function GetListDetailByNilaiId(Request $request)
    {

        $postData = $request->all();

        $listData = ViewNilaiDetail::getByNilaiId($postData["ID"]);

        if (is_null($listData)) {
            $listData = "";
        }

        $data = array(
            'data' => $listData
        );


        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function Edit(Request $request)
    {

        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $result = $this->GetDataByID($ID);
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Delete(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = NilaiModels::where('nilai_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function DeleteDetail(Request $request)
    {
        if (Session::get('is_login')) {

            $data =  $request->all();
            $ID = $data['id'];
            $stat = NilaiDetailModels::where('nilai_detail_id', $ID)
                ->update(['is_delete' => 1]);
            if ($stat > 0) {
                $result['status'] = true;
                $result['messages'] =  MessageEnum::success_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            } else {
                $result['status'] = false;
                $result['messages'] =  MessageEnum::failed_delete()->getValue();
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::not_authorized()->getValue();

            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function Save(Request $request)
    {
        $dataPOST = $request->all();
        $ID = $dataPOST['id'];
        $arrPostData = [];
        parse_str($dataPOST["data"], $arrPostData);
        $data = $this->MapToObj($arrPostData, $ID);

        $inserted_id = null;
        if (isset($data) && !is_null($data)) {
            if (!empty($ID)) {
                $result = $this->UpdateData($data);
                $inserted_id = $data['nilai_id'];
            } else {
                $inserted_id = $this->InsertData($data);
            }
            $result['status'] = true;
            if ($result['status']) {
                if (empty($ID)) {
                    // $data["nilai_id"] = ViewNilai::getByCode($data["nilai_code"])["ID"];
                    $data["nilai_id"] = $inserted_id;
                }
                $dataDetail = $this->MapToObjDetail(json_decode($dataPOST["detail"]), $data["nilai_id"]);
                for ($i = 0; $i < count($dataDetail); $i++) {
                    if ($result['status']) {
                        if (!is_null($dataDetail[$i]["nilai_detail_id"])) {
                            $result = $this->UpdateDataDetail($dataDetail[$i]);
                        } else {
                            $result =  $this->InsertDataDetail($dataDetail[$i]);
                        }
                    }
                }
            }
        } else {
            $result['status'] = false;
            $result['messages'] =   MessageEnum::failed_save()->getValue();
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function InsertData($data)
    {
        $nilai = new NilaiModels();

        $nilai->kelas_id = empty($data['kelas_id']) ? "" : $data['kelas_id'];
        $nilai->siswa_id = empty($data['siswa_id']) ? "" : $data['siswa_id'];
        $nilai->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $nilai->created_date = date("Y-m-d H:i:s");
        $nilai->created_by = Session::get('username');
        $stat = $nilai->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            //return $result;
            return $nilai->id;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            //return $result;
            return null;
        }

        return null;
    }

    private function InsertDataDetail($data)
    {
        $nilai_detail = new NilaiDetailModels();

        $nilai_detail->nilai_id = empty($data['nilai_id']) ? "" : $data['nilai_id'];
        $nilai_detail->mata_pelajaran_id = empty($data['mata_pelajaran_id']) ? "" : $data['mata_pelajaran_id'];
        $nilai_detail->nilai = empty($data['nilai']) ? 0 : $data['nilai'];
        $nilai_detail->keterangan = empty($data['keterangan']) ? "" : $data['keterangan'];

        $nilai_detail->created_date = date("Y-m-d H:i:s");
        $nilai_detail->created_by = Session::get('username');
        $stat = $nilai_detail->save();

        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }

        return null;
    }

    private function UpdateData($data)
    {
        $updateDetails = [
            'kelas_id' => $data['kelas_id'],
            'siswa_id' => $data['siswa_id'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = NilaiModels::where('nilai_id', $data['nilai_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }

    private function UpdateDataDetail($data)
    {
        $updateDetails = [
            'nilai_id' => $data['nilai_id'],
            'mata_pelajaran_id' => $data['mata_pelajaran_id'],
            'nilai' => $data['nilai'],
            'keterangan' => $data['keterangan'],

            'modified_date' => date("Y-m-d H:i:s"),
            'modified_by' =>  Session::get('username'),
        ];
        $stat = NilaiDetailModels::where('nilai_detail_id', $data['nilai_detail_id'])
            ->update($updateDetails);
        if ($stat > 0) {
            $result['status'] = true;
            $result['messages'] =  MessageEnum::success_save()->getValue();
            return $result;
        } else {
            $result['status'] = false;
            $result['messages'] =  MessageEnum::failed_save()->getValue();
            return $result;
        }
    }
    private function GetDataByID($ID)
    {
        $datanilai = ViewNilai::getByID($ID);
        $Data = array(
            'ID' => $datanilai["ID"],
            'Code' => $datanilai["code"],
            'Name' => $datanilai["name"],
            'Kelas_ID' => $datanilai["kelas_id"],
            'Kelas_Code' => $datanilai["kelas_code"],
            'Kelas_Name' => $datanilai["kelas_name"],
            'Kelas_Tingkat' => $datanilai["kelas_tingkat"],
            'Kelas_Tahun_Ajaran' => $datanilai["kelas_tahun_ajaran"],
            'Kelas_Semester' => $datanilai["kelas_semester"],
            'Siswa_ID' => $datanilai["siswa_id"],
            'Siswa_NIS' => $datanilai["siswa_nis"],
            'Siswa_Name' => $datanilai["siswa_name"],
            'Keterangan' => $datanilai["keterangan"],
        );
        return $Data;
    }

    private function MapToObj($Data, $ID)
    {
        $exsData = ViewNilai::getByID($ID);
        $retData = array();

        if (!is_null($exsData)) {
            $retData = array(
                'nilai_id' => $exsData["ID"],
                'kelas_id' => (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'siswa_id' => (!isset($Data["Siswa"])) ? null : ViewSiswa::getByNIS($Data["Siswa"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        } else {
            $retData = array(
                'nilai_id' => null,
                'kelas_id' => (!isset($Data["Kelas"])) ? null : ViewKelas::getByCode($Data["Kelas"])["ID"],
                'siswa_id' => (!isset($Data["Siswa"])) ? null : ViewSiswa::getByNIS($Data["Siswa"])["ID"],
                'keterangan' => (!isset($Data["Keterangan"])) ? "" : $Data["Keterangan"],
            );
        }

        return $retData;
    }

    private function MapToObjDetail($Data, $ID)
    {
        $listData = [];
        foreach ($Data as $value) {

            $exsData = ViewNilaiDetail::getByID($value->ID);
            $newData = array();

            if (!is_null($exsData)) {
                $newData = array(
                    'nilai_detail_id' => $exsData["ID"],
                    'nilai_id' => $ID,
                    'mata_pelajaran_id' => ViewMataPelajaran::getByCode($value->mata_pelajaran_code)["ID"],
                    'nilai' => (!isset($value->nilai)) ? 0 : $value->nilai,
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            } else {
                $newData = array(
                    'nilai_detail_id' => null,
                    'nilai_id' => $ID,
                    'mata_pelajaran_id' => ViewMataPelajaran::getByCode($value->mata_pelajaran_code)["ID"],
                    'nilai' => (!isset($value->nilai)) ? 0 : $value->nilai,
                    'keterangan' => (!isset($value->keterangan)) ? "" : $value->keterangan,
                );
            }

            $listData[] = $newData;
        }
        return $listData;
    }


    private function MapToSelectList($Data)
    {
        $retData = [];
        $defaultItem = new SelectList();
        $defaultItem->value = "";
        $defaultItem->text = "-- Select Item --";
        $retData[] = $defaultItem;

        if (!is_null($Data)) {
            foreach ($Data as $value) {
                $item = new SelectList();

                $item->value = $value->code;
                $item->text = $value->name;
                $retData[] = $item;
            }
        }
        return $retData;
    }

    private function ShowAlert($result)
    {

        if (Session::has('alert'))
            Session::forget('alert');

        if (Session::has('alert_types'));
        Session::forget('alert_types');

        if (Session::has('alert_messages'));
        Session::forget('alert_messages');

        Session::put('alert', true);
        Session::put('alert_types', ($result['status']) ? "Success" : "Error");
        Session::put('alert_messages', $result['messages']);
    }
}
