<?php

namespace App\Enums;

use Spatie\Enum\Enum;

abstract class ConfigEnum extends Enum
{
    public static function app_name(): ConfigEnum
    {
        return new class() extends ConfigEnum {
            public function getIndex(): int
            {
                return 1;
            }

            public function getValue(): string
            {
                return 'Sistem Informasi';
            }
        };
    }

    public static function company_name(): ConfigEnum
    {
        return new class() extends ConfigEnum {
            public function getIndex(): int
            {
                return 2;
            }

            public function getValue(): string
            {
                return 'SDN Pengalangan Gresik ';
            }
        };
    }
}
