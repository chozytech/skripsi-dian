<?php

namespace App\Enums;

use Spatie\Enum\Enum;

abstract class PageEnum extends Enum
{
    public static function login_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 1;
            }

            public function getValue(): string
            {
                return 'Masuk';
            }
        };
    }

    public static function dashboard_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 2;
            }

            public function getValue(): string
            {
                return 'Beranda';
            }
        };
    }

    public static function master_user_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 3;
            }

            public function getValue(): string
            {
                return 'Master Pengguna';
            }
        };
    }
    public static function master_guru_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 4;
            }

            public function getValue(): string
            {
                return 'Master Guru';
            }
        };
    }
    public static function master_kelas_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 5;
            }

            public function getValue(): string
            {
                return 'Master Kelas';
            }
        };
    }
    public static function master_siswa_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 6;
            }

            public function getValue(): string
            {
                return 'Master Siswa';
            }
        };
    }
    public static function master_wali_murid_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 7;
            }

            public function getValue(): string
            {
                return 'Master Wali Murid';
            }
        };
    }

    public static function master_mata_pelajaran_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 8;
            }

            public function getValue(): string
            {
                return 'Master Mata Pelajaran';
            }
        };
    }
    public static function trans_pengumuman_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 9;
            }

            public function getValue(): string
            {
                return 'Pengumuman';
            }
        };
    }
    public static function trans_jadwal_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 10;
            }

            public function getValue(): string
            {
                return 'Jadwal Pelajaran';
            }
        };
    }
    public static function trans_jurnal_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 11;
            }

            public function getValue(): string
            {
                return 'Jurnal';
            }
        };
    }
    public static function trans_kehadiran_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 12;
            }

            public function getValue(): string
            {
                return 'Kehadiran';
            }
        };
    }
    public static function trans_nilai_page(): PageEnum
    {
        return new class() extends PageEnum {
            public function getIndex(): int
            {
                return 13;
            }

            public function getValue(): string
            {
                return 'Nilai';
            }
        };
    }
}
