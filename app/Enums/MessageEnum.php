<?php

namespace App\Enums;

use Spatie\Enum\Enum;

abstract class MessageEnum extends Enum
{
    public static function login_failed(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 1;
            }

            public function getValue(): string
            {
                return 'Login gagal, username / password salah.';
            }
        };
    }

    public static function login_success(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 2;
            }

            public function getValue(): string
            {
                return 'Login berhasil, Selamat datang ';
            }
        };
    }

    public static function not_authorized(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 3;
            }

            public function getValue(): string
            {
                return 'Not Authorized.';
            }
        };
    }
    public static function success_delete(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 4;
            }

            public function getValue(): string
            {
                return 'Berhasil dihapus.';
            }
        };
    }
    public static function failed_delete(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 6;
            }

            public function getValue(): string
            {
                return 'Gagal dihapus.';
            }
        };
    }
    public static function already_exists(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 7;
            }

            public function getValue(): string
            {
                return 'Sudah digunakan.';
            }
        };
    }
    public static function success_save(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 8;
            }

            public function getValue(): string
            {
                return 'Berhasil menyimpan.';
            }
        };
    }
    public static function failed_save(): MessageEnum
    {
        return new class() extends MessageEnum {
            public function getIndex(): int
            {
                return 9;
            }

            public function getValue(): string
            {
                return 'Gagal menyimpan.';
            }
        };
    }
}
