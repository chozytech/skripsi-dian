<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListKelasSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_kelas_siswa AS SELECT
        ks.kelas_siswa_id    AS ID,
        k.kelas_id   AS kelas_id,
        k.kelas_code AS kelas_code,
        k.kelas_name AS kelas_name,
        k.kelas_tingkat AS kelas_tingkat,
        k.kelas_tahun_ajaran AS kelas_tahun_ajaran,
        k.kelas_semester AS kelas_semester,
        s.siswa_id   AS siswa_id,
        s.siswa_nis AS siswa_nis,
        s.siswa_name AS siswa_name,
        ks.keterangan AS keterangan
    FROM (tb_kelas_siswa ks
        LEFT JOIN tb_kelas k
        ON (ks.kelas_id = k.kelas_id)
        LEFT JOIN tb_siswa s
        ON (ks.siswa_id = s.siswa_id))
    WHERE ks.is_delete = 0
        AND ks.kelas_siswa_id >  - 1
        ORDER BY k.kelas_tahun_ajaran DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_kelas_siswa');
    }
}
