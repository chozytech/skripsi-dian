<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_level', function (Blueprint $table) {
            $table->bigIncrements('level_id');
            $table->string('level_code',50);
            $table->string('level_name',100);
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
        });

        DB::statement('ALTER TABLE tb_level CHANGE level_id level_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_level');
    }
}
