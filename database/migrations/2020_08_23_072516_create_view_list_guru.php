<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListGuru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_guru AS SELECT
        tb_guru.guru_id   AS ID,
        tb_guru.guru_nip AS nip,
        tb_guru.guru_name AS name,
        tb_guru.guru_phone AS phone,
        tb_guru.guru_email AS email,
        tb_guru.keterangan AS keterangan
      FROM tb_guru
      WHERE tb_guru.is_delete = 0
          AND tb_guru.guru_id >  - 1
      ORDER BY tb_guru.guru_name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_guru');
    }
}
