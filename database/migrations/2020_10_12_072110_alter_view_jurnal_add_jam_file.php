<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterViewJurnalAddJamFile extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    DB::statement("CREATE OR REPLACE VIEW view_list_jurnal AS 
        SELECT
        `j`.`jurnal_id`   AS `ID`,
  DATE_FORMAT(`j`.`jurnal_tanggal`,'%d/%m/%Y ') AS `tanggal`,
  `j`.`jurnal_jam`  AS `jam`,
  `j`.`jurnal_file` AS `file`,
  CONCAT(`u`.`first_name`,' (',`j`.`created_by`,')') AS `created_by`,
  CONCAT(`u2`.`first_name`,' (',`j`.`modified_by`,')') AS `modified_by`,
  `s`.`siswa_id`    AS `siswa_id`,
  `s`.`siswa_nis`   AS `siswa_nis`,
  `s`.`siswa_name`  AS `siswa_name`,
  `j`.`catatan`     AS `catatan`,
  `j`.`keterangan`  AS `keterangan`
FROM (((`tb_jurnal` `j`
     LEFT JOIN `tb_siswa` `s`
       ON (`j`.`siswa_id` = `s`.`siswa_id`))
    LEFT JOIN `tb_user` `u`
      ON (`j`.`created_by` = `u`.`username`
          AND `u`.`is_delete` = 0))
   LEFT JOIN `tb_user` `u2`
     ON (`j`.`modified_by` = `u2`.`username`
         AND `u2`.`is_delete` = 0))
WHERE `j`.`is_delete` = 0
    AND `j`.`jurnal_id` >  - 1
ORDER BY `j`.`jurnal_id` DESC");
  }



  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
  }
}
