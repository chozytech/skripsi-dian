<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbKelasSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_kelas_siswa', function (Blueprint $table) {
            $table->bigIncrements('kelas_siswa_id');
            $table->bigInteger('kelas_id');
            $table->bigInteger('siswa_id');
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('kelas_id')->references('kelas_id')->on('tb_kelas');
            $table->foreign('siswa_id')->references('siswa_id')->on('tb_siswa');
        });

        DB::statement('ALTER TABLE tb_kelas_siswa CHANGE kelas_siswa_id kelas_siswa_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_kelas_siswa');
    }
}
