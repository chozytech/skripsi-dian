<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPengumumanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pengumuman', function (Blueprint $table) {
            $table->bigIncrements('pengumuman_id');
            $table->string('pengumuman_code',50);
            $table->string('pengumuman_name',100);
            $table->text('pengumuman_file');
            $table->bigInteger('kelas_id')->nullable();
            $table->boolean('is_all')->default(false)->nullable();
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
        });

        DB::statement('ALTER TABLE tb_pengumuman CHANGE pengumuman_id pengumuman_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pengumuman');
    }
}
