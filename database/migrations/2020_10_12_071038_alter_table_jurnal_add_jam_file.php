<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableJurnalAddJamFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_jurnal', function($table) {
            $table->string('jurnal_jam',50);
            $table->text('jurnal_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_jurnal', function($table) {
            $table->dropColumn('jurnal_jam');
            $table->dropColumn('jurnal_file');
        });
    }
}
