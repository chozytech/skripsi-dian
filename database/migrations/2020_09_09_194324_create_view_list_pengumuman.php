<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListPengumuman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_pengumuman AS SELECT
        p.pengumuman_id    AS ID,
        p.pengumuman_code    AS code,
        p.pengumuman_name    AS name,
        p.pengumuman_file    AS `file`,
        p.is_all    AS is_all,
        k.kelas_id   AS kelas_id,
        k.kelas_code AS kelas_code,
        k.kelas_name AS kelas_name,
        k.kelas_tingkat AS kelas_tingkat,
        k.kelas_tahun_ajaran AS kelas_tahun_ajaran,
        k.kelas_semester AS kelas_semester,
        p.keterangan AS keterangan
    FROM tb_pengumuman p
        LEFT OUTER JOIN tb_kelas k
        ON (p.kelas_id = k.kelas_id)
    WHERE p.is_delete = 0
        AND p.pengumuman_id >  - 1
        ORDER BY p.pengumuman_id DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_pengumuman');
    }
}
