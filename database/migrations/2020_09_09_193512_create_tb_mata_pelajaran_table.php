<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMataPelajaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_mata_pelajaran', function (Blueprint $table) {
            $table->bigIncrements('mata_pelajaran_id');
            $table->string('mata_pelajaran_code',50);
            $table->string('mata_pelajaran_name',100);
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
        });

        DB::statement('ALTER TABLE tb_mata_pelajaran CHANGE mata_pelajaran_id mata_pelajaran_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_mata_pelajaran');
    }
}
