<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbKehadiranDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_kehadiran_detail', function (Blueprint $table) {
            $table->bigIncrements('kehadiran_detail_id');
            $table->bigInteger('kehadiran_id');
            $table->bigInteger('siswa_id');
            $table->boolean('is_send')->default(false);
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('kehadiran_id')->references('kehadiran_id')->on('tb_kehadiran');
            $table->foreign('siswa_id')->references('siswa_id')->on('tb_siswa');
        });

        DB::statement('ALTER TABLE tb_kehadiran_detail CHANGE kehadiran_detail_id kehadiran_detail_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_kehadiran_detail');
    }
}
