<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbJadwalDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jadwal_detail', function (Blueprint $table) {
            $table->bigIncrements('jadwal_detail_id');
            $table->bigInteger('jadwal_id');
            $table->bigInteger('mata_pelajaran_id');
            $table->string('jadwal_hari',50);
            $table->string('jadwal_jam',50);
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('jadwal_id')->references('jadwal_id')->on('tb_jadwal');
            $table->foreign('mata_pelajaran_id')->references('mata_pelajaran_id')->on('tb_mata_pelajaran');
        }); 
        DB::statement('ALTER TABLE tb_jadwal_detail CHANGE jadwal_detail_id jadwal_detail_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jadwal_detail');
    }
}
