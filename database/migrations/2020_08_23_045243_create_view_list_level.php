<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_level AS SELECT
        tb_level.level_id   AS ID,
        tb_level.level_code AS code,
        tb_level.level_name AS name,
        tb_level.keterangan AS keterangan
      FROM tb_level
      WHERE tb_level.is_delete = 0
          AND tb_level.level_id >  - 1
      ORDER BY tb_level.level_name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_level');
    }
}
