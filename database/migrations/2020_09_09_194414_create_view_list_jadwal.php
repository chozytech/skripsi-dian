<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListJadwal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_jadwal AS 
        SELECT
          j.jadwal_id AS ID,
          j.jadwal_code AS code,
          j.jadwal_name AS name,
           k.kelas_id           AS kelas_id,
          k.kelas_code         AS kelas_code,
          k.kelas_name         AS kelas_name,
          k.kelas_tingkat      AS kelas_tingkat,
          k.kelas_tahun_ajaran AS kelas_tahun_ajaran,
          k.kelas_semester     AS kelas_semester,
          j.keterangan   AS keterangan
        FROM tb_jadwal j
        LEFT JOIN tb_kelas k ON (j.kelas_id = k.kelas_id AND k.is_delete = 0)
        WHERE j.is_delete = 0
            AND j.jadwal_id >  - 1
        ORDER BY j.jadwal_id DESC");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_jadwal');
    }
}
