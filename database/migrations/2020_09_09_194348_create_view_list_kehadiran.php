<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListKehadiran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_kehadiran AS SELECT
        k.kehadiran_id    AS ID,
        DATE_FORMAT(k.kehadiran_tanggal,'%d/%m/%Y ') AS tanggal,
        kl.kelas_id   AS kelas_id,
        kl.kelas_code AS kelas_code,
        kl.kelas_name AS kelas_name,
        kl.kelas_tingkat AS kelas_tingkat,
        kl.kelas_tahun_ajaran AS kelas_tahun_ajaran,
        kl.kelas_semester AS kelas_semester,
        k.keterangan AS keterangan
    FROM tb_kehadiran k
    INNER JOIN tb_kelas kl ON (k.kelas_id = kl.kelas_id AND kl.is_delete = 0)
    WHERE k.is_delete = 0
        AND k.kehadiran_id >  - 1
        ORDER BY k.kehadiran_id DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_kehadiran');
    }
}
