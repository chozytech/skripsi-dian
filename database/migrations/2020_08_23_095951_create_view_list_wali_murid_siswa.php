<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListWaliMuridSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_wali_murid_siswa AS SELECT
        wms.wali_murid_siswa_id    AS ID,
        wm.wali_murid_id   AS wali_murid_id,
        wm.wali_murid_code AS wali_murid_code,
        wm.wali_murid_name AS wali_murid_name,
        wm.wali_murid_phone AS wali_murid_phone,
        wm.wali_murid_email AS wali_murid_email,
        wm.wali_murid_alamat AS wali_murid_alamat,
        s.siswa_id   AS siswa_id,
        s.siswa_nis AS siswa_nis,
        s.siswa_name AS siswa_name,
        wms.keterangan AS keterangan
    FROM (tb_wali_murid_siswa wms
        LEFT JOIN tb_wali_murid wm
        ON (wms.wali_murid_id = wm.wali_murid_id)
        LEFT JOIN tb_siswa s
        ON (wms.siswa_id = s.siswa_id))
    WHERE wms.is_delete = 0
        AND wms.wali_murid_siswa_id >  - 1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_wali_murid_siswa');
    }
}
