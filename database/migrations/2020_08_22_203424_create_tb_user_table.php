<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_user', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->string('username',50);
            $table->string('password',100);
            $table->string('first_name',100)->nullable();
            $table->string('last_name',100)->nullable();
            $table->bigInteger('level_id');
            $table->string('phone',20)->nullable();
            $table->string('email',100)->nullable();
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('level_id')->references('level_id')->on('tb_level');
        });

        DB::statement('ALTER TABLE tb_user CHANGE user_id user_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user');
    }
}
