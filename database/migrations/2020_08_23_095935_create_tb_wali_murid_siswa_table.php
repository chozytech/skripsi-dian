<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbWaliMuridSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_wali_murid_siswa', function (Blueprint $table) {
            $table->bigIncrements('wali_murid_siswa_id');
            $table->bigInteger('wali_murid_id');
            $table->bigInteger('siswa_id');
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('wali_murid_id')->references('wali_murid_id')->on('tb_wali_murid');
            $table->foreign('siswa_id')->references('siswa_id')->on('tb_siswa');
        });

        DB::statement('ALTER TABLE tb_wali_murid_siswa CHANGE wali_murid_siswa_id wali_murid_siswa_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_wali_murid_siswa');
    }
}
