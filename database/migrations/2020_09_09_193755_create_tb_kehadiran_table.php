<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbKehadiranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_kehadiran', function (Blueprint $table) {
            $table->bigIncrements('kehadiran_id');
            $table->dateTime('kehadiran_tanggal')->useCurrent();
            $table->bigInteger('kelas_id');
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('kelas_id')->references('kelas_id')->on('tb_kelas');
        });

        DB::statement('ALTER TABLE tb_kehadiran CHANGE kehadiran_id kehadiran_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_kehadiran');
    }
}
