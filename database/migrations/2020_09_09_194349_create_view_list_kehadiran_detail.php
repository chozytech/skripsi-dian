<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListKehadiranDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_kehadiran_detail AS 
        SELECT
          kd.kehadiran_detail_id AS ID,
           k.kehadiran_id AS kehadiran_id,
          DATE_FORMAT(k.kehadiran_tanggal,'%d/%m/%Y ') AS kehadiran_tanggal,
        s.siswa_id              AS siswa_id,
          s.siswa_nis             AS siswa_nis,
          s.siswa_name            AS siswa_name,
          kd.keterangan   AS keterangan
        FROM tb_kehadiran_detail kd
        LEFT JOIN tb_kehadiran k ON (kd.kehadiran_id = k.kehadiran_id AND k.is_delete = 0)
        LEFT JOIN tb_siswa s
             ON (kd.siswa_id = s.siswa_id)
        WHERE kd.is_delete = 0
            AND kd.kehadiran_detail_id >  - 1
        ORDER BY kd.kehadiran_detail_id DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_kehadiran_detail');
    }
}
