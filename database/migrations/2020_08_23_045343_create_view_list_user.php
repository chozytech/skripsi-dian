<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_user AS SELECT
                        u.user_id    AS user_id,
                        u.username   AS username,
                        u.password   AS password,
                        u.first_name AS first_name,
                        u.last_name  AS last_name,
                        l.level_id   AS level_id,
                        l.level_code AS level_code,
                        l.level_name AS level_name,
                        u.phone      AS phone,
                        u.email      AS email,
                        u.keterangan AS keterangan
                    FROM (tb_user u
                        LEFT JOIN tb_level l
                        ON (u.level_id = l.level_id))
                    WHERE u.is_delete = 0
                        AND u.user_id >  - 1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_user');
    }
}
