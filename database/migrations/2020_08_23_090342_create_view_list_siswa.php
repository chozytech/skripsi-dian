<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_siswa AS SELECT
        tb_siswa.siswa_id   AS ID,
        tb_siswa.siswa_nis AS nis,
        tb_siswa.siswa_name AS name,
        DATE_FORMAT(tb_siswa.siswa_tanggal_lahir,'%d/%m/%Y ') AS tanggal_lahir,
        tb_siswa.siswa_tempat_lahir AS tempat_lahir,
        tb_siswa.siswa_agama AS agama,
        tb_siswa.siswa_jenis_kelamin AS jenis_kelamin,
        tb_siswa.siswa_fingerprint AS fingerprint,
        tb_siswa.keterangan AS keterangan
      FROM tb_siswa
      WHERE tb_siswa.is_delete = 0
          AND tb_siswa.siswa_id >  - 1
      ORDER BY tb_siswa.siswa_name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_siswa');
    }
}
