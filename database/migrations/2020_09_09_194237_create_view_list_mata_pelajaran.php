<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListMataPelajaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_mata_pelajaran AS SELECT
        tb_mata_pelajaran.mata_pelajaran_id   AS ID,
        tb_mata_pelajaran.mata_pelajaran_code AS code,
        tb_mata_pelajaran.mata_pelajaran_name AS name,
        tb_mata_pelajaran.keterangan AS keterangan
      FROM tb_mata_pelajaran
      WHERE tb_mata_pelajaran.is_delete = 0
          AND tb_mata_pelajaran.mata_pelajaran_id >  - 1
      ORDER BY tb_mata_pelajaran.mata_pelajaran_name");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_mata_pelajaran');
    }
}
