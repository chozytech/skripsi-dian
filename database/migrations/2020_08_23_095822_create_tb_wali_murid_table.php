<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbWaliMuridTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_wali_murid', function (Blueprint $table) {
            $table->bigIncrements('wali_murid_id');
            $table->string('wali_murid_code',50);
            $table->string('wali_murid_name',100);
            $table->string('wali_murid_phone',20);
            $table->string('wali_murid_email',100);
            $table->text('wali_murid_alamat');
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
        });

        DB::statement('ALTER TABLE tb_wali_murid CHANGE wali_murid_id wali_murid_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_wali_murid');
    }
}
