<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListNilaiDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_nilai_detail AS 
        SELECT
          nd.nilai_detail_id    AS ID,
          n.nilai_id            AS nilai_id,
          nd.nilai           AS nilai,
          mp.mata_pelajaran_id   AS mata_pelajaran_id,
          mp.mata_pelajaran_code AS mata_pelajaran_code,
          mp.mata_pelajaran_name AS mata_pelajaran_name,
          nd.keterangan          AS keterangan
        FROM ((tb_nilai_detail nd
            LEFT JOIN tb_nilai n
              ON (nd.nilai_id = n.nilai_id
                  AND n.is_delete = 0))
           LEFT JOIN tb_mata_pelajaran mp
             ON (nd.mata_pelajaran_id = mp.mata_pelajaran_id
                 AND mp.is_delete = 0))
        WHERE nd.is_delete = 0
            AND nd.nilai_detail_id >  - 1
        ORDER BY nd.nilai_detail_id DESC");
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_nilai_detail');
    }
}
