<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListNilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_nilai AS 
        SELECT
          n.nilai_id          AS ID,
           s.siswa_id           AS siswa_id,
          s.siswa_nis          AS siswa_nis,
          s.siswa_name         AS siswa_name,
          k.kelas_id   AS kelas_id,
        k.kelas_code AS kelas_code,
        k.kelas_name AS kelas_name,
        k.kelas_tingkat AS kelas_tingkat,
        k.kelas_tahun_ajaran AS kelas_tahun_ajaran,
        k.kelas_semester AS kelas_semester,
          n.keterangan         AS keterangan
        FROM ((tb_nilai n
           LEFT JOIN tb_siswa s
             ON (n.siswa_id = s.siswa_id
                 AND s.is_delete = 0))
            LEFT JOIN tb_kelas k
             ON (n.kelas_id = k.kelas_id
                 AND k.is_delete = 0))
        WHERE n.is_delete = 0
            AND n.nilai_id >  - 1
        ORDER BY n.nilai_id");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_nilai');
    }
}
