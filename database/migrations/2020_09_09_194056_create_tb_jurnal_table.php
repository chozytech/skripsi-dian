<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbJurnalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jurnal', function (Blueprint $table) {
            $table->bigIncrements('jurnal_id');
            $table->dateTime('jurnal_tanggal')->useCurrent();
            $table->bigInteger('siswa_id');
            $table->text('catatan')->nullable();
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('siswa_id')->references('siswa_id')->on('tb_siswa');
        }); 
        DB::statement('ALTER TABLE tb_jurnal CHANGE jurnal_id jurnal_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jurnal');
    }
}
