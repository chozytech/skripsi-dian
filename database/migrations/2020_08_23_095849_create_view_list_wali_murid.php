<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListWaliMurid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_wali_murid AS SELECT
        tb_wali_murid.wali_murid_id   AS ID,
        tb_wali_murid.wali_murid_code AS code,
        tb_wali_murid.wali_murid_name AS name,
        tb_wali_murid.wali_murid_phone AS phone,
        tb_wali_murid.wali_murid_email AS email,
        tb_wali_murid.wali_murid_alamat AS alamat,
        tb_wali_murid.keterangan AS keterangan
      FROM tb_wali_murid
      WHERE tb_wali_murid.is_delete = 0
          AND tb_wali_murid.wali_murid_id >  - 1
      ORDER BY tb_wali_murid.wali_murid_name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_wali_murid');
    }
}
