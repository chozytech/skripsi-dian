<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListJadwalDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_jadwal_detail AS 
        SELECT
          jd.jadwal_detail_id    AS ID,
          j.jadwal_id            AS jadwal_id,
          j.jadwal_code          AS jadwal_code,
          j.jadwal_name          AS jadwal_name,
          jd.jadwal_hari          AS hari,
          jd.jadwal_jam          AS jam,
          mp.mata_pelajaran_id   AS mata_pelajaran_id,
          mp.mata_pelajaran_code AS mata_pelajaran_code,
          mp.mata_pelajaran_name AS mata_pelajaran_name,
          jd.keterangan          AS keterangan
        FROM ((tb_jadwal_detail jd
            LEFT JOIN tb_jadwal j
              ON (jd.jadwal_id = j.jadwal_id
                  AND j.is_delete = 0))
           LEFT JOIN tb_mata_pelajaran mp
             ON (jd.mata_pelajaran_id = mp.mata_pelajaran_id
                 AND mp.is_delete = 0))
        WHERE jd.is_delete = 0
            AND jd.jadwal_detail_id >  - 1
        ORDER BY jd.jadwal_detail_id DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_jadwal_detail');
    }
}
