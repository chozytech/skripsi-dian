<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbNilaiDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_nilai_detail', function (Blueprint $table) {
            $table->bigIncrements('nilai_detail_id');
            $table->bigInteger('nilai_id');
            $table->bigInteger('mata_pelajaran_id');
            $table->integer('nilai')->default(0);
            $table->string('created_by',50)->default("admin");
            $table->dateTime('created_date')->useCurrent();
            $table->string('modified_by',50)->default("admin");
            $table->dateTime('modified_date')->useCurrent();
            $table->boolean('is_delete')->default(false);
            $table->text('keterangan')->nullable();
            $table->foreign('nilai_id')->references('nilai_id')->on('tb_nilai');
            $table->foreign('mata_pelajaran_id')->references('mata_pelajaran_id')->on('tb_mata_pelajaran');
        }); 
        DB::statement('ALTER TABLE tb_nilai_detail CHANGE nilai_detail_id nilai_detail_id BIGINT (20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_nilai_detail');
    }
}
