<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewListKelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW view_list_kelas AS SELECT
        tb_kelas.kelas_id   AS ID,
        tb_kelas.kelas_code AS code,
        tb_kelas.kelas_name AS name,
        tb_kelas.kelas_tingkat AS tingkat,
        tb_kelas.kelas_tahun_ajaran AS tahun_ajaran,
        tb_kelas.kelas_semester AS semester,
        tb_kelas.keterangan AS keterangan
      FROM tb_kelas
      WHERE tb_kelas.is_delete = 0
          AND tb_kelas.kelas_id >  - 1
      ORDER BY tb_kelas.kelas_name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_list_kelas');
    }
}
