<?php

use Illuminate\Database\Seeder;

class tb_user_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('tb_user')->insert([
        	'user_id' => 1,
        	'username' => 'admin',
            'password' =>  md5('admin'),
            'first_name' => 'Administrator',
            'level_id' => 1,
        ]);
        
    }
}
