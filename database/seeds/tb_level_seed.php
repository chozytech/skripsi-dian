<?php

use Illuminate\Database\Seeder;

class tb_level_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_level')->insert([
            'level_id' => 1,
        	'level_code' => 'admin',
        	'level_name' => 'Administrator',
        ]);
        DB::table('tb_level')->insert([
            'level_id' => 2,
        	'level_code' => 'guru',
        	'level_name' => 'Guru',
        ]);
        DB::table('tb_level')->insert([
            'level_id' => 3,
        	'level_code' => 'wali_murid',
        	'level_name' => 'Wali Murid',
        ]);
    }
}
